@extends('layouts.admin_master')
@section('main_content')
    <div class="card">
        <div class="body"> 
            <h5>Medicine List</h5>
            <h2>
                <a id="btn_add" name="btn_add" class="btn btn-secondary">
                    <i style="color:#000000" class="material-icons">add_circle</i></a>
            </h2>
            <div class="table-responsive">
                <table id="example" class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Type</th>
                        <th>Generic</th>
                        <th>Code</th>
                        <th>Rack</th>
                        <th>Quantity</th>
                        <th>Purchase</th>
                        <th>Sale</th>
                        <th>Effect</th>
                        <th>Expire</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Company</th>
                        <th>Type</th>
                        <th>Generic</th>
                        <th>Code</th>
                        <th>Rack</th>
                        <th>Quantity</th>
                        <th>Purchase</th>
                        <th>Sale</th>
                        <th>Effect</th>
                        <th>Expire</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody id="products-list" name="products-list">
                    @foreach($medicine as $product)
                        <tr id="product{{$product->id}}">
                            <td>{{$product->id}}</td>
                            <td>{{$product->medicine_name}}</td>
                            <td>{{$product->medicine_company}}</td>
                            <td>{{$product->medicine_type}}</td>
                            <td>{{$product->generic}}</td>
                            <td>{{$product->medicine_code}}</td>
                            <td>{{$product->medicine_rack}}</td>
                            <td>{{$product->medicine_quantity}}</td>
                            <td>{{$product->purchase_price}}</td>
                            <td>{{$product->sell_price}}</td>
                            <td>{{$product->medicine_effect}}</td>
                            <td>{{$product->expire_date}}</td>
                            <td>
                                <button class="btn btn-warning btn-detail open_modal" value="{{$product->id}}">Edit</button>
                                <button  class="btn btn-danger btn-delete delete-product" value="{{$product->id}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Passing BASE URL to AJAX -->
    <input id="url" type="hidden" value="{{ \Request::url() }}">
    <!-- MODAL SECTION -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="material-icons">note_add</i>
                        Add Medicine
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="frmProducts">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Medicine Name</label>
                                <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Required field">
                            </div>
                            <div class="col-sm-6">
                                <label>Medicine Company</label>
                                <input type="text" class="form-control" name="medicine_company" id="medicine_company" placeholder="Required field">
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Medicine Type</label>
                                <select required class="form-control" id="medicine_type" name="medicine_type">
                                    <option value="SelectTypes">Select Type</option>
                                    <option>Type1</option>
                                    <option>type2</option>
                                    <option>Type3</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Generic</label>
                                    <select  name="generic" id="generic" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                     @foreach($generics as $generic)
                                        <option value="{{$generic->generic_name}}">{{$generic->generic_name}}</option>
                                     @endforeach
                                    </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Code</label>
                                <input type="text" class="form-control" name="medicine_code" id="medicine_code" placeholder="Optional field">
                            </div>
                            <div class="col-sm-6">
                                <label>Rack</label>
                                <input type="text" class="form-control" name="medicine_rack" id="medicine_rack" placeholder="Optional field">
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Piece in a box</label>
                                <input type="number" class="form-control" name="medicine_quantity" id="medicine_quantity" placeholder="Only Number">
                            </div>
                            <div class="col-sm-4">
                                <label>Purchase Price</label>
                                <input type="number" class="form-control" name="purchase_price" id="purchase_price" placeholder="Only Number">
                            </div>
                            <div class="col-sm-4">
                                <label>Sale Price</label>
                                <input type="number" class="form-control" name="sell_price" id="sell_price" placeholder="Only Number">
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Effect</label>
                                <input type="text" class="form-control" name="medicine_effect" id="medicine_effect" placeholder="Effect">
                            </div>
                            <div class="col-sm-6">
                                <label>Expire Date</label>
                                <input type="date" class="form-control" name="expire_date" id="expire_date">
                                {{--<input type="text" class="datepicker form-control" placeholder="Please choose a date...">--}}
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="btn-save" class="btn btn-primary">Save</button>
                    <input type="hidden" id="product_id" name="product_id" value="0">
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection