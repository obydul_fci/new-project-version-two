@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        User Log History List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>User IP</th>
                                <th>Login</th>
                                <th>Logout</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>User IP</th>
                                <th>Login</th>
                                <th>Logout</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($userHistorys as $history)
                                <tr>
                                    <td>{{$history->full_name}}</td>
                                    <td>{{$history->user_ip}}</td>
                                    <td>{{$history->login_time}}</td>
                                    <td>{{$history->logout_time}}</td>
                                    <td>{{$history->status}}</td>
                                    <td>
                                        <a href="{{ URL::to('single_user_log_history/' . $history->user_id) }}" class="dropdown-toggle btn btn-info"><i class="material-icons">visibility</i> View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection