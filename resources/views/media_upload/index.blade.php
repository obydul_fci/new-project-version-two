@extends('layouts.admin_master')
@section('main_content')
    <h5>Upload Media</h5>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="{{ route('media_file.store') }}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input class="form-control" type="file" name="image[]" multiple required>
                <br>
                <button type="submit" class="btn btn-info">Upload</button>
            </form>
        </div>
    </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                MEDIA LIBRARY
                            </h2>
                        </div>
                        <div class="body">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                @if($file->isEmpty())
                                    <strong style="color:red">No Media found</strong>
                                @else
                                    @foreach($file as $files)
                                <div class="col-md-2">
                                    <a href="{{url('public/uploads/media/'.$files->image)}}" data-sub-html="Demo Description">
                                        <img width="100%" height="200px" src="{{url('public/uploads/media/'.$files->image)}}">
                                    </a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['media_file.destroy', $files->id],'style'=>'display:inline']) !!}
                                    <button style="margin:4px 0;" class="btn btn-danger btn-delete" type="submit" onclick="return confirm('Are you sure?')" style="display: inline;"
                                    >Remove</button>
                                    {!! Form::close() !!}
                                </div>
                                    @endforeach
                                @endif
                            </div>
                            {{$file->links()}}
                        </div>
                    </div>
                </div>
            </div>
@endsection