@extends('layouts.admin_master')
@section('main_content')
    <!-- Advanced Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>USER PROFILE</h2>
                </div>
                <div class="body">
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Upload Profile image</h4>
                                </div>
                                <div class="modal-body">
                                    {{ Form::open(array('action' => 'Admin\UserProfileController@update','class' =>'form_advanced_validation','method' => 'post','files' => true)) }}
                                    <input type="file" name="user_profile_image" id="user_profile_image" required><br>
                                    <button class="btn btn-info" type="submit" name="profile-upload">Upload</button>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                {!! Form::open(['url' => 'profile','class' => 'form_advanced_validation','method' => 'post','files' => true]) !!}
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="{{$profile->full_name}}">
                            <label class="form-label">Name</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <a href="" data-toggle="modal" data-target="#mymedia">Upload from media</a>
                            or
                            <a href="" data-toggle="modal" data-target="#myModal">Select Image</a>
                            <div id="mymedia" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Select Media file</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if($media->isEmpty())
                                                <strong style="color:red">No Media found</strong>
                                            @else
                                                @foreach($media as $medias)
                                                    <div class=" col-md-2 image-style">
                                                        {{ Html::image('public/uploads/media/'.$medias->image, 'alt', array( 'width' => 100, 'height' => 100,)) }}
                                                        <input type="checkbox" name="check_list" id="{{$medias->id}}" value="{{$medias->image}}" class="filled-in chk-col-pink">
                                                        <label for="{{$medias->id}}"></label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        @if($media->isEmpty())
                                            <a class="btn btn-primary" href="{{url('media_file')}}">Upload media</a>
                                            @else
                                            <div class="modal-footer">
                                                <button id="reset" class="btn btn-info" type="submit" name="check">Add</button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <label>Select Gender</label>
                    <div class="md-radio">
                        <input id="1" type="radio" name="g" value="1" checked>
                        <label for="1">Male</label>
                    </div>
                    {{--<div class="md-radio">--}}
                        {{--<input id="2" type="radio" name="g" value="2">--}}
                        {{--<label for="2">Female</label>--}}
                    {{--</div>--}}
                    <div class="md-radio">
                        <input id="3" type="radio" name="g" value="2">
                        <label for="3">Female</label>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="address" value="{{$profile->address}}">
                            <label class="form-label">Address</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="mobile" value="{{$profile->mobile}}">
                            <label class="form-label">Mobile</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <label class="form-label">Date of birth</label>
                        <div class="form-line">
                            <input id="date" type="date" name="date" value="{{$profile->birth_date}}">
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="profession" value="{{$profile->profession}}">
                            <label class="form-label">Profession</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="division" value="{{$profile->division}}">
                            <label class="form-label">Division</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="district" value="{{$profile->district}}">
                            <label class="form-label">District</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="upazila" value="{{$profile->upzila}}">
                            <label class="form-label">Upazila</label>
                        </div>
                    </div>
                    <button class="btn btn-primary waves-effect" name="profile-info-update" type="submit">UPDATE</button>
                    <!--   </form> -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Advanced Validation -->
@endsection