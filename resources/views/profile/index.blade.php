@extends('layouts.admin_master')
@section('main_content')
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading">Profile Image</div>
            <div class="panel-body">
                @if($profile->picture=='NULL' && $profile->gender==1)
                    <img src="uploads/user/user_mail.png" width="80" height="80" alt="Your Picture" />
                @elseif($profile->picture=='NULL' && $profile->gender==2)
                    <img src="uploads/user/user_femail.png" width="80" height="80" alt="Your Picture" />
                @else
                    <img src="public/uploads/media/{{$profile->picture}}" width="80" height="80" alt="Your Picture" />
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">Profile Information</div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Mobile</th>
                        <th>Gender</th>
                        <th>Birth</th>
                        <th>Profession</th>
                        <th>Division</th>
                        <th>District</th>
                        <th>Upazila</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$profile->full_name}}</td>
                        <td>{{$profile->email}}</td>
                        <td>{{$profile->address}}</td>
                        <td>{{$profile->mobile}}</td>
                        <td>
                            @if($profile->gender==1)
                                Male
                            @elseif($profile->gender==2)
                                Female
                            @else
                                Shemale
                            @endif
                        </td>
                        <td>{{$profile->birth_date}}</td>
                        <td>{{$profile->profession}}</td>
                        <td>{{$profile->division}}</td>
                        <td>{{$profile->district}}</td>
                        <td>{{$profile->upzila}}</td>
                    </tr>
                    </tbody>
                </table>
                 <a class="btn btn-primary" href="{{url('profile/update')}}">Update Profile</a>
            </div>
        </div>
    </div>
@endsection