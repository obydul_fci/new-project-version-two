@extends('layouts.register_master')

@section('content')
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>Pharmacy</b></a>
            <small>Please login your account</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{ url('user_login') }}">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="{{url('user_reg')}}">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="{{ url('') }}">Forgot Password?</a>
                        </div><br/>
                        <a href="{{url('auth/facebook')}}" class="btn btn-bg bg-pink" style="margin-bottom: 5px">Facebook</a>
                        <a href="{{url('auth/linkedin')}}" class=" btn btn-bg bg-pink" style="margin-bottom: 5px">Linkdin</a>
                        <br><br>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
