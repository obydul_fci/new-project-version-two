@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        User List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            {{--@foreach($user_activations as $user_activation)--}}
                                {{--<tr>--}}
                                {{--<td>{{$user_activation->full_name}}</td>--}}
                                {{--<td>{{$user_activation->role_name}}</td>--}}
                                {{--<td>--}}
                            {{--{!! Form::open(['route' =>['user_activation.update',$user_activation->id],'class' => 'form_advanced_validation','method' => 'PUT','files' => true]) !!}--}}
                                {{--<select id="completed" class="form-select" name="completed" onClick="this.form.submit()">--}}
                                {{--<option value="1">Active</option>--}}
                                {{--<option value="0" >InActive</option>--}}
                                {{--</select>--}}
                            {{--@if($user_activation->status ==1 )--}}
                                {{--<button class="btn btn-danger waves-effect" name="status" value="2" style="margin-left: 5px" onClick="this.form.submit()"> <i class="material-icons">edit</i> Inactive</button>--}}
                                {{--@else--}}
                                {{--<button class="btn btn-primary waves-effect" name="status" value="1" style="margin-left: 5px" onClick="this.form.submit()"> <i class="material-icons">edit</i> Active</button>--}}
                                {{--@endif--}}
                                {{--{!! Form::close() !!}--}}

                                {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection