@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        User List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr>
                            <td>{{$user_activations->full_name}}</td>
                            <td>{{$user_activations->role_name}}</td>
                            <td>
                            {!! Form::open(['route' =>['user_activation.update',$user_activations->id],'class' => 'form_advanced_validation','method' => 'PUT','files' => true]) !!}
                            @if($user_activations->status ==1 )
                            <button class="btn btn-danger waves-effect" name="status" value="2" style="margin-left: 5px" onClick="this.form.submit()">Inactive</button>
                            @else
                            <button id="active_user_id" class="btn btn-primary waves-effect" name="status" value="1" style="margin-left: 5px" onclick="return confirm('are you sure?')"> Active</button>
                            @endif
                            {!! Form::close() !!}
                            </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection