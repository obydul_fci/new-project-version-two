@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <i class="material-icons">account_box</i>
                    <h5>
                        User List
                    </h5>
                    <ul class="header-dropdown m-r--5">
                        <a data-toggle="modal" data-target="#userAdd" class="btn btn-secondary"><i style="color:#000000" class="material-icons">add_circle</i></a>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->full_name}}</td>
                                    <td>{{$user->role_name}}</td>
                                    <td>
                                        {!! Form::open(['route' =>['user_activation.update',$user->id],'class' => 'form_advanced_validation','method' => 'PUT','files' => true]) !!}
                                        @if($user->status ==1 )
                                            <button class="btn btn-danger btn-sm" name="status" value="2" style="margin-left: 5px" onClick="return confirm('Are you sure?')"><i class="material-icons">highlight_off</i> Inactive</button>
                                        @else
                                            <button class="btn btn-primary waves-effect" name="status" value="1" style="margin-left: 5px" onClick="return confirm('Are you sure?')"><i class="material-icons">check_circle</i> Active</button>
                                        @endif
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    <!-- user add modal --->
    <!-- Modal -->
    <div class="modal fade" id="userAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> User Add</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' =>'add_user','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                    {{ Form::bsText('full_name') }}
                    {{ Form::bsEmail('email') }}
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password" type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="radio" name="gender" value="1" id="male" class="with-gap" checked>
                        <label for="male">Male</label>

                        <input type="radio" name="gender" value="2" id="female" class="with-gap">
                        <label for="female" class="m-l-20">Female</label>
                    </div>
                    <button type="button" class="btn btn-secondary btn-md" data-dismiss="modal"><i class="material-icons">highlight_off</i> Close</button>&nbsp&nbsp
                    {{ Form::bsSubmit('Submit') }}
                    {!! Form::close() !!}
                </div>
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                    {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@endsection