@extends('layouts.admin_master')
@section('main_content')

    <!-- Modal -->
    <div class="modal fade" id="roleModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Role Create</h4>
                </div>
                <div class="modal-body">
                    <!-- Advanced Validation -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">

                                    {!! Form::open(['route' =>'roles.store','class' => 'form_advanced_validation','method' => 'POST']) !!}
                                    {{ Form::bsText('slug') }}
                                    {{ Form::bsText('name') }}
                                    {{ Form::bsText('permissions') }}
                                    {{ Form::bsSubmit('Submit') }}
                                    {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Advanced Validation -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <i class="material-icons">account_box</i>
                    <h5>
                        Role List
                    </h5>
                    <?php $array = json_decode($userPermissionAccess->permissions);
                    if (is_array($array) || is_object($array))
                    {
                        foreach ($array as $value)
                        {
                    $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                            if($cleanStr == 'create'){
                                ?>
                    <ul class="header-dropdown m-r--5">
                        <a data-toggle="modal" data-target="#roleModal" class="btn btn-secondary"><i style="color:#000000" class="material-icons">add_circle</i></a>
                    </ul>
                    <?php
                            }
                        }
                    }
                    ?>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Slug</th>
                                <th>Name</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Slug</th>
                                <th>Name</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($roles as $role)

                            <tr>
                                <td>{{$role->slug}}</td>
                                <td>{{$role->name}}</td>
                                <td>
                                    <?php $array = json_decode($role->permissions);
                                    if (is_array($array) || is_object($array))
                                    {
                                        $row_count = 0;
                                        foreach ($array as $value)
                                        {
                                            if($row_count > 0) {
                                                echo ", ";
                                            }
                                            $row_count++;
                                            echo $value;

                                        }
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php $array = json_decode($userPermissionAccess->permissions);
                                    if (is_array($array) || is_object($array))
                                    {
                                        foreach ($array as $value)
                                        {
                                            $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                                            if($cleanStr == 'update'){
                                                ?>
                                    <a class="btn btn-primary waves-effect" href="{{ URL::to('roles/' . $role->id . '/edit') }}" style="margin-left: 5px"> Edit</a>
                                        <?php
                                        }
                                        }
                                        }
                                        ?>
                                        <?php $array = json_decode($userPermissionAccess->permissions);
                                        if (is_array($array) || is_object($array))
                                        {
                                        foreach ($array as $value)
                                        {
                                        $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                                        if($cleanStr == 'delete'){
                                        ?>
                                    {{ Form::open(array('url' => 'roles/' . $role->id, 'class' => 'pull-left')) }}
                                    {{ Form::hidden('_method', 'DELETE') }}
                                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                    {{ Form::close() }}
                                        <?php
                                        }
                                        }
                                        }
                                        ?>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection