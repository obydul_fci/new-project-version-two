@extends('layouts.admin_master')
@section('main_content')
    <!-- Advanced Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Role Edit Form</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('roles')}}" class="btn btn-primary"><i class="material-icons" style="color: white;">settings_backup_restore</i> Back
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    {!! Form::open(['route' =>['roles.update',$role->id],'class' => 'form_advanced_validation','method' => 'PUT','files' => true]) !!}
                    {{ Form::bsText('slug',$role->slug) }}
                    {{ Form::bsText('name',$role->name) }}
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="permissions" value="<?php $array = json_decode($role->permissions);
                            if (is_array($array) || is_object($array))
                            {
                                $row_count = 0;
                                foreach ($array as $value)
                                {
                                    if($row_count > 0) {
                                        echo ", ";
                                    }
                                    $row_count++;
                                    echo $value;

                                }
                            }
                            ?>" maxlength="50" minlength="2" required="" aria-required="true">
                            <label class="form-label">Min/Max Length</label>
                        </div>
                        <div class="help-info">Min. 3, Max. 10 characters</div>
                    </div>
                    {{--{{ Form::bsText('permissions',$role->permissions) }}--}}
                    {{ Form::hidden('_method', 'PUT') }}
                    {{ Form::bsSubmit('Submit') }}
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Advanced Validation -->
@endsection