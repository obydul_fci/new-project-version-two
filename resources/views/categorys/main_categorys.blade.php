@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Main Category List
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mainCategoryAdd"><i class="material-icons">add_box</i> New Add</button>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($mainCategorys as $category)
                                <tr>
                                    <td>{{$category->category_name}}</td>
                                    <td>{{$category->slug}}</td>
                                    <td>
                                        {!! Form::open(['url' =>['categorys_update',$category->id],'class' => 'form_advanced_validation','method' => 'post','files' => true]) !!}
                                        @if($category->status == 1)
                                            <button class="btn btn-danger waves-effect" name="status" value="0" style="margin-left: 5px" onClick="this.form.submit()"><i class="material-icons">highlight_off</i> Inactive</button>
                                            @else
                                            <button class="btn btn-primary waves-effect" name="status" value="1" style="margin-left: 5px" onClick="this.form.submit()"><i class="material-icons">check_circle</i> Active</button>
                                            @endif
                                        {!! Form::close() !!}
                                    </td>
                                    <td>
                                        <button class="pull-left btn btn-primary waves-effect" data-toggle="modal" data-target="#mainCategoryEdit{{$category->id}}"
                                                style="margin-left: 5px">Edit</button>
                                        {{ Form::open(array('url' => 'categorys/' . $category->id, 'class' => 'pull-left')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Delete', array('class' => 'btn btn-danger','style' =>'margin-left:10px')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>

                                <!-- Modal header edit -->
                                <div class="modal fade" id="mainCategoryEdit{{$category->id}}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Category Edit Form</h4>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['route' =>['categorys.update',$category->id],'class' => 'form_advanced_validation','method' => 'PUT','files' => true]) !!}
                                                {{ Form::bsText('category_name',$category->category_name) }}
                                                {{ Form::bsText('slug',$category->slug) }}
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <select class="form-control show-tick" name="parent_id">
                                                            <option value="">-- Please Select Parent --</option>
                                                            @foreach($mainCategorys as $parents)
                                                                <option value="{{$parents->id}}" @if ($category->id == $parents->id) selected="selected" @endif>{{$parents->category_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                {{ Form::hidden('_method', 'PUT') }}
                                                {{ Form::bsSubmit('Submit') }}
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>

                        <!-- Modal main category add -->
                        <div class="modal fade" id="mainCategoryAdd" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Main Category Add Form</h4>
                                    </div>
                                    <div class="modal-body">
                                        {!! Form::open(['route' =>'categorys.store','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                                        {{ Form::bsText('category_name') }}
                                        {{ Form::bsText('slug') }}
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <select class="form-control show-tick" name="parent_id">
                                                    <option value="">-- Please Select Parent --</option>
                                                    @foreach($mainCategorys as $parent)
                                                        <option value="{{$parent->id}}" @if (old('parent_id') == $parent->id) selected="selected" @endif>{{$parent->category_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{ Form::bsSubmit('Submit') }}
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
@endsection