@extends('layouts.admin_master')
@section('main_content')

    <!-- Advanced Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Purchase Report</h2>
                </div>
                <div class="body">

                    <form id="modalFormReset">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Select Supplier</label>
                                <select  name="supplier" id="supplier" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                    <option value="0">All</option>
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->full_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-6">
                                {{--<div id="txtHint" class="title-color" style="padding-top:50px; text-align:center;" ><b>Blogs information will be listed here...</b></div>--}}
                                <label>Select Items</label>
                                <select  name="medicine" id="medicine" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                    <option value="0">All</option>
                                    @foreach($medicines as $medicine)
                                    <option value="{{$medicine->id}}">{{$medicine->medicine_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Start Date</label>
                                <input type="date" class="form-control" name="purchase_date" id="purchase_date">
                            </div>

                            <div class="col-sm-6">
                                {{--<div id="txtHint" class="title-color" style="padding-top:50px; text-align:center;" ><b>Blogs information will be listed here...</b></div>--}}
                                <label>End Date</label>
                                <input type="date" class="form-control" name="purchase_date" id="purchase_date">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Report Type</label>
                                <select  name="modal_medicine" id="modal_medicine" class="selectpicker form-control" data-size="5">
                                    <option value="0">All</option>
                                    <option value="1">EXCEL</option>
                                    <option value="2">CSV</option>
                                    <option value="3">PDF</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <button type="button" class="btn btn-primary" id="salesReturnAdd">Sales Return Add</button>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Advanced Validation -->


@endsection