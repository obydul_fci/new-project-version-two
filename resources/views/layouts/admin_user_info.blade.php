<div class="user-info">
    <div class="image">
        <?php
        $profile =DB::table('users')->where('id',Sentinel::getUser()->id)->first();
        ?>
        @if( Sentinel::check())
            @if(Sentinel::getUser()->picture=='NULL' && Sentinel::getUser()->gender==1)
              <img src="{{URL::asset('/uploads/user/user_mail.png')}}" width="48" height="48" alt="Your Picture" />
            @elseif(Sentinel::getUser()->picture=='NULL' && Sentinel::getUser()->gender==2)
                <img src="uploads/user/user_femail.png" width="48" height="48" alt="Your Picture" />
            @else
             {{ Html::image('public/uploads/media/'.$profile->picture, 'alt', array( 'width' => 48, 'height' => 48,)) }}
            @endif
        @endif
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if( Sentinel::check())
                {{Sentinel::getUser()->full_name}}
            @endif
        </div>
        <div class="email">
            @if( Sentinel::check())
                {{Sentinel::getUser()->email}}
            @endif
        </div>
        <div class="btn-group user-helper-dropdown">
            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
            <ul class="dropdown-menu pull-right">
                <li><a href="{{url('profile')}}"><i class="material-icons">person</i>Profile</a></li>
                <li role="seperator" class="divider"></li>
                <li><a href="{{url('user_logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
            </ul>
        </div>
    </div>
</div>