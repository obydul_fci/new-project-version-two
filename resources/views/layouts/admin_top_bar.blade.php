<nav class="navbar">

    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{url('dashboard')}}">
                   PHARMACY
                </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                <!-- #END# Call Search -->
                <!-- Notifications -->
                <li class="dropdown">
                    @if($count > 0)
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">
                            {{$count}}
                        </span>
                        </a>
                        @else
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">
                        </span>
                        </a>
                        @endif
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu">
                                @if($user_list_notification->isEmpty())
                                      <span style="color:red;font-weight:bold;margin: 0 85px;">No Notification</span>

                                @else
                                @foreach($user_list_notification as $users_notify)
                                <li>
                                    <a href="{{url('show_user/'.$users_notify->id)}}">
                                        <div class="icon-circle bg-light-green">
                                            <i class="material-icons">person_add</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4><span style="color:red;">{{$users_notify->full_name}}</span> new members joined</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> {{$users_notify->created_at->diffForHumans() }}

                                            </p>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                                @endif
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-cyan">--}}
                                            {{--<i class="material-icons">add_shopping_cart</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4>4 sales made</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 22 mins ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View All Notifications</a>
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
            </ul>
        </div>
    </div>
</nav>