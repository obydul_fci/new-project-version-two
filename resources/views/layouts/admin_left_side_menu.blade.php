<div class="menu">
    <ul class="list">
        <li class="header">PHARMACY NAVIGATION</li>
        <li class="active">
            <a href="{{url('dashboard')}}">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        @if( Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='admin')
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">account_balance</i>
                    <span>Account</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/account')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Chart Of account</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/journal')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Journal Entry</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/purchase')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>purchase</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('sales')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Sales</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/salesReturn')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Sales Return</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">account_balance</i>
                    <span>Report</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{url('/purchasesReport')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Purchase Report</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/sales')}}">
                            <i class="material-icons">insert_chart</i>
                            <span>Sales</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{url('medicine')}}">
                    <i class="material-icons">note_add</i>
                    <span>Medicine</span>
                </a>
            </li>
            <li>
                <a href="{{url('generics')}}">
                    <i class="material-icons">note_add</i>
                    <span>Generic</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">account_circle</i>
                    <span>User Log</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a  href="{{url('user_activation')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>User List</span>
                        </a>
                    </li>
                    <li>
                        <a  href="{{url('all_user_log_history')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>User History</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('roles')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>Role</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('roleAssigns')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>Role Users</span>
                        </a>
                    </li>
                    <li id="show_data">
                        <a href="{{url('permissions')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>Permissions</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('permissionAssigns')}}">
                            <i class="material-icons">perm_identity</i>
                            <span>Assign Permissions</span>
                        </a>
                    </li>
                </ul>
            </li>
        <li>
            <a href="{{url('media_file')}}">
                    <i class="material-icons">add_a_photo</i>
                    <span>Media</span>
            </a>
        </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">perm_data_setting</i>
                    <span>Category</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <li>
                        <a href="{{url('categorys')}}">
                            <i class="material-icons">copyright</i>
                            <span>Category</span>
                        </a>
                        </li>
                    <li>
                        <a href="{{url('subcategorys')}}">
                            <i class="material-icons">copyright</i>
                            <span>Sub Category</span>
                        </a>
                    </li>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{url('cachClear')}}">
                    <i class="material-icons">restore_page</i>
                    <span>Clear</span>
                </a>
            </li>
        @endif
        @if( Sentinel::check() && Sentinel::getUser()->roles()->first()->slug =='manager')
        <li>
            <a href="#">
                <i class="material-icons">layers</i>
                <span>Admin</span>
            </a>
        </li>
        @endif
        @if( Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='pharmacist')
            <li>
                <a href="#">
                    <i class="material-icons">layers</i>
                    <span>User</span>
                </a>
            </li>
        @endif
        @if( Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='salesman')
            <li>
                <a href="#">
                    <i class="material-icons">layers</i>
                    <span>User</span>
                </a>
            </li>
        @endif
    </ul>
</div>