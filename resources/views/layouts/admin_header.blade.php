<title>Pharmacy</title>
<!-- Google Fonts -->
<link href="{{URL::asset('assets/plugins/google_fonts/google_font_one_css.css')}} " rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/plugins/google_fonts/google_font_icon.css')}} " rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="{{URL::asset('assets/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="{{URL::asset('assets/plugins/node-waves/waves.css')}}" rel="stylesheet" />

<link href="{{URL::asset('assets/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<!-- Animation Css -->
<link href="{{URL::asset('assets/plugins/animate-css/animate.css')}}" rel="stylesheet" />

<!-- JQuery DataTable Css -->
<link href="{{URL::asset('assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<!-- Custom Css -->
<link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet">
<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="{{URL::asset('assets/css/themes/all-themes.css')}}" rel="stylesheet" />
<!-- Toust Flash message -->
<link href="{{URL::asset('assets/css/toastr.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<!-- Sweet alert message-->
<link href="{{URL::asset('assets/js/sweetalert/sweetalert.css')}}" rel="stylesheet">
<script src="{{URL::asset('assets/js/sweetalert/sweetalert.min.js')}}"></script>
<script src=" {{URL::asset('assets/js/sweetalert/jquery-2.1.3.min.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script src="{{asset('js/ajaxscript.js')}}"></script>

<script src="{{asset('js/journal_masterScript.js')}}"></script>

<script src="{{asset('js/coa-head.js')}}"></script>
<style type="text/css">
    .image-style img{
        padding: 15px;
    }
    .image-style [type="checkbox"] + label{
        position: relative;
        padding-left: 35px;
        cursor: pointer;
        display: inline-block;
        height: 9px;
        line-height: 25px;
        font-size: 1rem;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
        bottom: 57px;
        margin-left: 40px;
    }
    .bootstrap-select .bs-searchbox .form-control,
    .bootstrap-select .bs-actionsbox .form-control,
    .bootstrap-select .bs-donebutton .form-control{
        margin-left: 25px !important;
    }

</style>



