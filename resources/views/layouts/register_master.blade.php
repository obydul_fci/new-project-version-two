<!DOCTYPE html>
<html>
<head>
    @include('layouts.admin_header')
</head>
<body class="signup-page login-page fp-page">
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
        @yield('content')
</body>
<!-- Tuest Flash message show -->
<script type="text/javascript" src="{{URL::asset('assets/js/toastr/toastr.min.js')}}"></script>
<!-- Flash message shwo -->
@if(Session::has('success'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.success('{{ Session::get('success') }}');
            //alert("hello world");

        }, 1300);
    </script>
@elseif(Session::has('error'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.error('{{ Session::get('error') }}');

        }, 1300);
    </script>
@elseif(Session::has('warning'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.warning('{{ Session::get('warning') }}');
        }, 1300);
    </script>
@else(Session::has('voucher'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            //toastr.success('<a href="{{ Session::get('id') }}">{{ Session::get('voucher') }} | Print Voucher222</a>');

        }, 1300);
    </script>
@endif
<!--End flash message show -->
</html>