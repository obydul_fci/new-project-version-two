
<!-- Jquery Core Js -->
<script src="{{URL::asset('assets/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{URL::asset('assets/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
{{--<script src="{{URL::asset('assets/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>--}}

<!-- Slimscroll Plugin Js -->
<script src="{{URL::asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{URL::asset('assets/plugins/node-waves/waves.js')}}"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{URL::asset('assets/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<!-- Extra JavaScript/CSS added manually in "Settings" tab -->
<!-- Custom Js -->
<script src="{{URL::asset('assets/js/admin.js')}}"></script>
<script src="{{URL::asset('assets/js/pages/tables/jquery-datatable.js')}}"></script>



<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


<!-- Include jQuery -->
{{--<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>--}}
{{--<!-- Include Date Range Picker -->--}}
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>--}}
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>--}}


<!-- Demo Js -->
<script src="{{URL::asset('assets/js/demo.js')}}"></script>
<!-- Tuest Flash message show -->
<script type="text/javascript" src="{{URL::asset('assets/js/toastr/toastr.min.js')}}"></script>
<!-- Flash message shwo -->
@if(Session::has('success'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.success('{{ Session::get('success') }}');
            //alert("hello world");

        }, 1300);
    </script>
@elseif(Session::has('error'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.error('{{ Session::get('error') }}');

        }, 1300);
    </script>
@elseif(Session::has('warning'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            toastr.warning('{{ Session::get('warning') }}');
        }, 1300);
    </script>
@else(Session::has('voucher'))
    <script type="text/javascript">
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                // progressBar: true,
                showMethod: 'slideDown',
                timeOut: 7000
            };
            //toastr.success('<a href="{{ Session::get('id') }}">{{ Session::get('voucher') }} | Print Voucher222</a>');

        }, 1300);
    </script>
@endif
<!--End flash message show -->
<!--category wise sub category  show -->
<script type="text/javascript">
    $('#main_category_id').change(function(){
        var categoryID = $(this).val();
        if(categoryID){
            $.ajax({
                type:"GET",
                url:"{{url('api_sub_subcategorys_show')}}?main_category_id="+categoryID,
                success:function(res){
                    if(res){
                        $("#subcategory_id").empty();
                        $("#subcategory_id").append('<option>-- Please Select subcategory --</option>');
                        $.each(res,function(key,value){
                            $("#subcategory_id").append('<option value="'+key+'">'+value+'</option>');
                        });

                    }else{
                        alert('ok');
                        $("#subcategory_id").empty();
                    }
                }
            });
        }else{
            $("#subcategory_id").empty();
            $("#upazila").empty();
        }
    });
</script>
<!--bootstrap date picker plugin -->
<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
<!--End category wise sub category  show -->