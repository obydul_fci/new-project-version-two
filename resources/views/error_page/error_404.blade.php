<!DOCTYPE html>
<html>

<head>
    @include('layouts.admin_header')
</head>

<body class="four-zero-four">
<div class="four-zero-four-container">
    <div class="error-code">404</div>
    <div class="error-message">You are not authorized to access this page</div>
    <div class="button-place">
        <a href="{{url('dashboard')}}" class="btn btn-default btn-lg waves-effect">GO TO HOMEPAGE</a>
    </div>
</div>

@include('layouts.admin_footer_link')
</body>

</html>