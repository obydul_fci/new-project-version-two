@extends('layouts.admin_master')
@section('main_content')

    <!-- Advanced Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Sales Return</h2>
                </div>
                <div class="body">

                    <form id="modalFormReset">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Sales Return No</label>
                                <input type="text" class="form-control cal_val" name="sales_return_no" id="sales_return_no" placeholder="please enter invoice no">
                            </div>

                            <div class="col-sm-6">
                                {{--<div id="txtHint" class="title-color" style="padding-top:50px; text-align:center;" ><b>Blogs information will be listed here...</b></div>--}}
                                <label>Medicine</label>
                                <select class=" form-control" name="modal_medicine" id="modal_medicine" >
                                </select>

                            </div>
                        </div>
                    </form>
                    <button type="button" class="btn btn-primary" id="salesReturnAdd">Sales Return Add</button>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Advanced Validation -->

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Sales Return List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        {!! Form::open(['url' =>'salesReturnStore','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                        {{--<form id="frmProducts">--}}
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Sales Return No</label>
                                <input type="text" class="form-control" name="selseReturnNo" id="selseReturnNo" placeholder="please enter invoice no">
                            </div>
                            <div class="col-sm-6">
                                <label>Sales Return Date</label>
                                <input type="date" class="form-control" name="salesReturnDate" id="salesReturnDate">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Notes</label>
                                <textarea name="notes" class="form-control" rows="5" id="notes"></textarea>
                            </div>
                        </div>


                        <table class="table table-bordered table-striped table-hover" id="list ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Sales Price</th>
                                <th>Total Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="addTblRow">
                            </tbody>
                            <tbody>

                            </tbody>
                        </table>

                        {{--</form>--}}
                        {{--{{ Form::bsSubmit('Submit') }}--}}
                        <button type="submit" class="btn btn-primary" id="salesReturnSubmit">Sales Return Add</button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    {{--payment type--}}


<!-- invoice wise medicine show-->
    <script>
        $(document).ready(function(){
            $("#sales_return_no").keyup(function(){
                var invoice_no = $("#sales_return_no").val();
                if(invoice_no) {
                    $.get( "{{ url('salesInvoiceWiseMedicine?id=') }}"+invoice_no, function(data) {
                        console.log(data);
                        $('#modal_medicine').empty();
                        $('#modal_medicine').append("<option value=''>" + 'Please select medicine' + "</option>")
                        $.each(data,function(index, child){
                            $('#modal_medicine').append('<option value="'+child.id+'">'+child.medicine_name+'</option>');
                        });

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            var i = 0;
            $("#salesReturnAdd").click(function(){
                i++;
                var x1 = document.getElementById("sales_return_no").value;
                var x2 = document.getElementById("modal_medicine").value;
                var ttt = "{{ url('invoiceWiseMedicineDetails?id=') }}"+x1+"&id2="+x2;
//                alert(ttt);
                if(x1) {
                    $.get( ttt, function(data) {
                        console.log(data);
                        $('#modal_medicine').empty();
                        $.each(data,function(index, child){
                            var rows = "";
                            rows += '<tr><td><input type="text" class="form-control" name="rows['+i+'][modal_medicine_name]" value=" '+ child.medicine_name +' " id="modal_medicine_name"><input type="hidden" class="form-control" name="rows['+i+'][salesMedicineId]" value=" '+ child.id +' " id="salesMedicineId"><input type="hidden" class="form-control" name="customerId" value=" '+ child.customer_id +' " id="customerId"></td><td><input type="text" class="form-control" name="rows['+i+'][salesQuantity]" value="'+ child.sales_quantity +' " id="salesQuantity"></td><td><input type="text" class="form-control" name="rows['+i+'][sales_price]" value=" '+ child.sales_price +' " id="sales_price"></td><td><input type="text" class="form-control" name="rows['+i+'][salesTotalPrice]" value=" '+ child.total_price +' " id="salesTotalPrice"></td><td><input type="button" value="Delete" id="addTblRow'+i+'"  onclick="deleteRow(this)"></td></tr>';
                            $(rows).appendTo("#addTblRow");
                        });

                    });
                }

            });
        });
    </script>
    <!-- javascript validation -->
    <script type="text/javascript">
        // journal entry validation
        $(document).ready(function() {
            $("#salesReturnSubmit").click(function(){
                var salesReturnNo = $("#selseReturnNo").val();
                var salesReturnDate = document.getElementById("salesReturnDate");
                var notes = $("#notes").val();

                if(salesReturnNo==''){
                    swal('Sales Return No Empty');
                    return false;
                }
                if(salesReturnDate.value=="salesReturnDate"){
                    swal('Select Sales Return Date');
                    return false;
                }
                if(notes==''){
                    swal('Notes No Empty');
                    return false;
                }
            });
        });
    </script>
@endsection