@extends('layouts.admin_master')
@section('main_content')
    <div class="card">
        <div class="body">
            <h5>Chart Of Account</h5>
            <h2>
                <a id="coa_add" name="coa_add" class="btn btn-secondary">
                    <i style="color:#000000" class="material-icons">add_circle</i></a>
            </h2>
            <div class="table-responsive">
                <table id="account_chart" class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Account Code</th>
                        <th>Account Name</th>
                        <th>Account Type</th>
                        <th>Account Balance</th>
                        <th>Parent</th>
                        <th>Note</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Modified By</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Account Code</th>
                        <th>Account Name</th>
                        <th>Account Type</th>
                        <th>Account Balance</th>
                        <th>Parent</th>
                        <th>Note</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Modified By</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody id="coa-list" name="coa-list">
                    @foreach($acc_head as $head)
                        <tr id="coa{{$head->id}}">
                            <td>{{$head->id}}</td>
                            <td>{{$head->acc_code}}</td>
                            <td>{{$head->acc_name}}</td>
                            <td>{{$head->acc_type}}</td>
                            <td>{{$head->openning_balance}}</td>
                            <td>@if($parentName = DB::table('coa-head')->where('id',$head->parent)->first())
                                    {{$parentName->acc_name}}
                                @endif
                            </td>
                            <td>{{$head->note}}</td>
                            <td>{{$head->status}}</td>
                            <td>{{$head->created_by}}</td>
                            <td>{{$head->modified_by}}</td>
                            <td>
                                <button class="btn btn-warning btn-detail chart_modal" value="{{$head->id}}">Edit</button>
                                <button class="btn btn-danger btn-delete delete-coa" value="{{$head->id}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Passing BASE URL to AJAX -->
    <input id="coa-url" type="hidden" value="{{ \Request::url() }}">
    <!-- MODAL SECTION -->
    <div class="modal fade" id="coa-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <i class="material-icons">account_balance</i>
                        <span>Chart Of Account</span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="coa-Products">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Account Code <span style="color:red">*</span></label>
                                <input type="text" class="form-control" name="acc_code" id="acc_code" placeholder="Account Code">
                            </div>
                            <div class="col-sm-6">
                                <label>Account Name <span style="color:red">*</span></label>
                                <input type="text" class="form-control" name="acc_name" id="acc_name" placeholder="Account Name">
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Account Type</label>
                                <select required class="form-control" id="acc_type" name="acc_type">
                                    <option value="0">Select Account Type</option>
                                    <option>Type1</option>
                                    <option>type2</option>
                                    <option>Type3</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Select Parent</label>
                                <select name="parent" id="parent" class="form-control">
                                    <option value="0">Root</option>;
                                    @foreach ($acc_head_two as $parent)
                                        <option value="{{$parent->id}}">{{$parent->acc_name}}</option>
                                        @foreach (DB::table('coa-head')->where('parent',$parent->id)->get() as $subcategory)
                                            <option value="{{$subcategory->id}}">-- {{$subcategory->acc_name}}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                                {{--<select name="parent" id="parent" class="form-control">--}}
                                {{--@foreach($acc_head as $parent)--}}
                                {{--<option value="{{$parent->id}}">{{$parent->acc_name}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                            </div>
                        </div>
                        <br><br>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                {{--<label>Generic</label>--}}
                                {{--<select  name="generic" id="generic" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">--}}
                                {{--@foreach($generics as $generic)--}}
                                {{--<option value="{{$generic->generic_name}}">{{$generic->generic_name}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                <label>Openning Balance</label>
                                <input type="text" class="form-control" name="openning_balance" id="openning_balance" placeholder="Openning Balance">
                            </div>
                            <div class="col-sm-6">
                                <label>Note</label>
                                <input type="text" class="form-control" name="note" id="note" placeholder="Note">
                            </div>
                            <input type="hidden" name="status" id="status" value="Active">
                            <input type="hidden" name="created_by" id="created_by" value="{{Sentinel::getUser()->full_name}}">
                            <input type="hidden" name="modified_by" id="modified_by" value="{{Sentinel::getUser()->full_name}}">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="coa-save" class="btn btn-primary">Save</button>
                    <input type="hidden" id="coa_id" name="coa_id" value="0">
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection