@extends('layouts.admin_master')
@section('main_content')
    <script type="text/javascript">
        //journal debit data
        $( document ).ready(function() {
            var i=0;
            var theTotal = '';
            $("#dev_check_id").click(function(){
                i++;
                var d_id = document.getElementById("deb_dev_id");
                var x1 = document.getElementById("deb_dev_id").value;
                var x2 = document.getElementById("debit_amount").value;
                theTotal = Number(theTotal) + Number($("#debit_amount").val());
                document.getElementById('debit_sum_amount_id').value=theTotal;
                if(d_id.value == "DebitType") {
                    swal("Select Debit Account Head!");
                    return false;
                }
                else if(x2==''){
                    swal('Enter Debit Amount');
                    return false;
                }else{
//                    var rows = "";
//                    var rows1="";
                      var rows3="";
//                    rows += '<input id="addTblRow" type="text" name="rows['+i+'][debit_acc_name]" class="form-control debit_acc" value="'+x1+'">';
//                    $(rows).appendTo("#addTblRow");
//                    rows1 += '<input id="addTblRow1" type="text" name="rows['+i+'][debit_amount_name]" class="form-control debit_amm" value="'+x2+'">';
//                    $(rows1).appendTo("#addTblRow1");
                    rows3+='<tr id=""><td><input id="addTblRow" type="text" name="rows['+i+'][debit_acc_name]" class="form-control debit_acc" value="'+x1+'"></td><td><input id="addTblRow1" type="text" name="rows['+i+'][debit_amount_name]" class="form-control debit_amm" value="'+x2+'"></td><td><button class="btn btn-danger">Remove</button></td></tr>';
                   $(rows3).appendTo("#d_accc_headsss");
                }
          });
            document.getElementById('debit_sum_amount_id').value=theTotal;
        });

        //journal credit data
        $( document ).ready(function() {
            var i=0;
            var theTotal1 = '';
            $("#cre_check_id").click(function(){
                i++;
                var c_id = document.getElementById("credit_sel_id");
                var x3 = document.getElementById("credit_sel_id").value;
                var x4 = document.getElementById("credit_amount").value;
                theTotal1 = Number(theTotal1) + Number($("#credit_amount").val());
                document.getElementById('credit_sum_amount_id').value=theTotal1;
                if(c_id.value == "CreditType") {
                    swal("Select Credit Account Head!");
                    return false;
                }
                else if(x4==''){
                    swal('Enter Credit Amount');
                }else{
//                    var rows3 = "";
//                    var rows4="";
                    var c_row="";
//                    rows3 += '<input id="addTblRow3" type="text" name="roww['+i+'][credit_acc_name]" class="form-control credit_acc" value="'+x3+'">';
//                    $(rows3).appendTo("#addTblRow3");
//                    rows4 += '<input id="addTblRow4" type="text" name="roww['+i+'][credit_amount_name]" class="form-control credit_amm" value="'+x4+'">';
//                    $(rows4).appendTo("#addTblRow4");
                    c_row+='<tr><td><input id="addTblRow3" type="text" name="roww['+i+'][credit_acc_name]" class="form-control credit_acc" value="'+x3+'"></td><td><input id="addTblRow4" type="text" name="roww['+i+'][credit_amount_name]" class="form-control credit_amm" value="'+x4+'"></td><td><button class="btn btn-danger">Remove</button></td></tr>';
                    $(c_row).appendTo("#c_accc_headsss");
                }
            });
            document.getElementById('credit_sum_amount_id').value=theTotal1;
        });

        // journal entry validation
        $(document).ready(function() {
           $("#test_journal").click(function(){
               var j_no=$("#journal_no").val();
               var j_date=$("#date").val();
               var d_a_c_id = document.getElementById("deb_dev_id");
               var c_a_c_id = document.getElementById("credit_sel_id");
               var debit_s_amount=$("#debit_amount").val();
               var credit_s_amount=$("#credit_amount").val();
               var debit_sum_amount_id=$("#debit_sum_amount_id").val();
               var credit_sum_amount_id=$("#credit_sum_amount_id").val();
               if(j_no==''){
                  swal('Journal No Empty');
                   return false;
               }
               if(j_date==''){
                   swal('Journal Date Empty');
                   return false;
               }
               if(d_a_c_id.value == "DebitType") {
                   swal("Select Debit Account Head!");
                   return false;
               }
               if(c_a_c_id.value == "CreditType") {
                   swal("Select Credit Account Head!");
                   return false;
               }
               if(debit_s_amount==''){
                   swal('Enter Debit Amount');
                   return false;
               }
               if(credit_s_amount==''){
                   swal('Enter Credit Amount');
                   return false;
               }
               if(debit_sum_amount_id==''){
                   swal('Debit Total Amount Is Empty');
                   return false;
               }
               if(credit_sum_amount_id==''){
                   swal('Credit Total Amount Is Empty');
                   return false;
               }
               if(credit_sum_amount_id!=debit_sum_amount_id){
                   swal('Debit And Credit Amount Must Be Equal');
                   return false;
               }
           });
        });
    </script>
    {!! Form::open(['url' =>'journal','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                  <div class="well">
                     <h4>JOURNAL ENTRY</h4>
                      <br>
                          <div class="com-md-12">
                              <div class="row clearfix">
                                  <div class="col-md-6 col-sm-6">
                                        <label>Journal No <span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="journal_no" id="journal_no" placeholder="Journal No">
                                  </div>
                                  <div class="col-md-6 col-sm-6">
                                      <label class="control-label" for="date">Journal Date <span style="color:red">*</span></label>
                                      <input class="form-control myclass" id="date" name="date" placeholder="DD/MM/YYY" type="text"/>
                                  </div>
                                  <br><br><br><br>
                                  <div class="col-md-12 col-sm-12">
                                          <label for="comment">Note:</label>
                                          <textarea class="form-control" name="notes" rows="5" id="notes"></textarea>
                                      <br><br>
                                  </div>
                                  <div class="col-md-12 col-sm-12">
                                      <label>Status</label>
                                      <div class="demo-switch">
                                          <div class="switch">
                                              <label>OFF<input type="checkbox" id="checkboxId" name="onoffswitch" checked><span class="lever"></span>ON</label>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <input type="hidden" name="user_type" id="user_type" value="{{Sentinel::getUser()->full_name}}">
                          </div>
                    </div>
                  <div class="well">
                      <div class="row clearfix">
                          <div class="col-md-6 col-sm-6">
                              <h4>Debit Voucher</h4>
                              <label>Select A/C Head <span style="color:red">*</span></label>
                              <select required id="deb_dev_id" name="deb_dev_id" class="form-control">
                               <option value="DebitType">Select Debit A/c Head</option>
                                  @foreach ($acc_head_two as $parent)
                                      <option value="{{$parent->id}}">{{$parent->acc_name}}</option>
                                      @foreach (DB::table('coa-head')->where('parent',$parent->id)->get() as $subcategory)
                                          <option value="{{$subcategory->id}}">-- {{$subcategory->acc_name}}</option>
                                      @endforeach
                                  @endforeach
                              </select>
                              <br>
                              <label>Debit Amount <span style="color:red">*</span></label>
                              <input type="text" id="debit_amount" name="debit_amount" class="form-control dmm">
                              <br><br>
                              <button type="button" class="btn btn-success" id="dev_check_id">Save</button>
                          </div>
                          <div class="col-md-6 col-sm-6">
                              <h4>Credit Voucher</h4>
                              <label>Select A/C Head <span style="color:red">*</span></label>
                              <select required id="credit_sel_id" name="credit_sel_id" class="form-control">
                                  <option value="CreditType">Select Credit A/c Head</option>
                                  @foreach ($acc_head_two as $parent)
                                      <option value="{{$parent->id}}">{{$parent->acc_name}}</option>
                                      @foreach (DB::table('coa-head')->where('parent',$parent->id)->get() as $subcategory)
                                          <option value="{{$subcategory->id}}">-- {{$subcategory->acc_name}}</option>
                                      @endforeach
                                  @endforeach
                              </select>
                              <br>
                              <label>Credit Amount <span style="color:red">*</span></label>
                              <input type="text" class="form-control" id="credit_amount">
                              <br><br>
                              <button type="button" id="cre_check_id" class="btn btn-success" type="button">Save</button>
                          </div>
                      </div>
                  </div>
                        <div class="card">
                            <div class="body">
                                <h4>Voucher List</h4>
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6">
                              <div class="table-responsive">
                                  <h5>Debit Voucher</h5>
                                    <table  class="table table-bordered table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Debit A/C Head</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="d_accc_headsss" name="coa-list">
                                       </tbody>
                                </table>
                                  <b>Total Amount:</b> <input style="border: 0px solid #ffffff;" type="text" name="total_debit_amounts" id="debit_sum_amount_id">
                           </div>
                         </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="table-responsive">
                                            <h5>Credit Voucher</h5>
                                            <table id="account_chart" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Credit A/C Head</th>
                                                    <th>Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="c_accc_headsss" name="coa-list">
                                                </tbody>
                                            </table>
                                            <b>Total Amount:</b> <input style="border: 0px solid #ffffff;" type="text" name="total_credit_amounts" id="credit_sum_amount_id">
                                      </div>
                             </div>
                     </div>
                   <button type="submit" class="btn btn-primary" id="test_journal">COMPLETE</button>
          {!! Form::close() !!}
              </div>
            </div>
@endsection