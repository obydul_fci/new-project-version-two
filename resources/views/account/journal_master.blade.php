@extends('layouts.admin_master')
@section('main_content')
    <!-- Advanced Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add journal master</h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a data-toggle="modal" data-target="#roleModal" class="btn btn-secondary">
                                    <i style="color:#000000" class="material-icons">add_circle</i>
                                </a>
                        </li>
                    </ul>
                </div>
                <div class="body">

                    <form id="frmProducts">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Journal No</label>
                                <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Required field">
                            </div>
                            <div class="col-sm-6">
                                <label>Journal Date</label>
                                <input type="date" class="form-control" name="expire_date" id="expire_date">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Notes</label>
                                <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Required field">
                            </div>
                            <div class="col-sm-6">
                                <label>Status</label>
                                <input type="text" class="form-control" name="medicine_company" id="medicine_company" placeholder="Required field">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- #END# Advanced Validation -->


    <!-- Modal -->
    <div class="modal fade" id="roleModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Role Create</h4>
                </div>
                <div class="modal-body">
                    <!-- Advanced Validation -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">

                                    <form id="frmProducts">
                                        <div class="row clearfix">
                                            <div class="col-sm-6">
                                                <label>Account No</label>
                                                <select  name="generic" id="generic" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                                    <option value="1">140</option>
                                                    <option value="2">141</option>
                                                    <option value="3">142</option>
                                                    <option value="4">143</option>
                                                    <option value="5">144</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>DR/CR</label>
                                                <select  name="generic" id="generic" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                                    <option value="1">Debit</option>
                                                    <option value="2">Credit</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-sm-6">
                                                <label>Amount</label>
                                                <input type="text" class="form-control" name="medicine_name" id="medicine_name" placeholder="Required field">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# Advanced Validation -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection