@extends('layouts.admin_master')
@section('main_content')

    <!-- Advanced Validation -->
    <!-- Add single purchase form -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Purchase</h2>
                </div>
                <div class="body">

                    <form id="modalFormReset">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Medicine</label>
                                <select  name="modal_medicine" id="modal_medicine" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                    <option value="0">Select Medicine</option>
                                    @foreach($medicines as $medicine)
                                        <option value="{{$medicine->id}}">{{$medicine->medicine_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label>Quantity</label>
                                <input type="text" class="form-control" name="modal_quantity" id="modal_quantity" placeholder="please enter quantity">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>	Purchase Price</label>
                                <input type="text" class="form-control cal_val" name="modal_purchase_price" id="modal_purchase_price" placeholder="please enter purchase price">
                            </div>
                            <div class="col-sm-6">
                                <label>Discount</label>
                                <select  name="modal_discount" id="modal_discount" class="selectpicker form-control"  data-show-subtext="true" data-live-search="true" data-size="5" onchange="showDivModal(this)">
                                    <option value="0">Select Discount</option>
                                    <option value="1">Amount</option>
                                    <option value="2">Percentage</option>
                                </select><br/><br/>
                                <input type="number" class="form-control cal_val" name="modal_discount_amount" id="modal_discount_amount" placeholder="please enter discount amount" style="display: none">
                                <input type="number" class="form-control cal_val" name="modal_discount_percentage" id="modal_discount_percentage" placeholder="please enter discount percentage" style="display: none">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Price</label>
                                <input type="text" class="form-control ap_total" name="price" id="price" readonly >
                            </div>
                        </div>

                    </form>
                    <button type="button" class="btn btn-primary" id="id1">Purchase Add</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Add single purchase form end -->
    <!-- #END# Advanced Validation -->

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Purchase List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        {!! Form::open(['url' =>'purchaseStore','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                        {{--<form id="frmProducts">--}}
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <label>Invoice No</label>
                                    <input type="text" class="form-control" name="invoice_no" id="invoice_no" placeholder="please enter invoice no">
                                </div>
                                <div class="col-sm-6">
                                    <label>Supplier Name</label>
                                    <select  name="supplier_name" id="supplier_name" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" data-size="5">
                                        <option value="supplier">Select Supplier</option>
                                        @foreach($suppliers as $suplier)
                                            <option value="{{$suplier->id}}">{{$suplier->full_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <label>Notes</label>
                                <textarea name="notes" class="form-control" rows="5" id="notes"></textarea>
                                {{--<input type="text" class="form-control" name="notes" id="notes" placeholder="please enter notes">--}}
                            </div>
                            <div class="col-sm-6">
                                <label>	Purchase Date</label>
                                <input type="date" class="form-control" name="purchase_date" id="purchase_date">
                            </div>
                        </div>


                        <table class="table table-bordered table-striped table-hover" id="list ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Purchase Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="addTblRow">
                            </tbody>
                            <tbody>
                            <tr>
                                <td colspan="4" style="text-align: center">Order Total</td>
                            </tr>
                            <tr>
                                <td>22</td>
                                <td>Total Quantity : <input type="number" id="total_quantity" readonly> </td>
                                <td>Total Price : <input type="number" id="total_price" readonly></td>
                                <td class="total"></td>
                            </tr>
                            <tr>
                                <td>.</td>
                                <td>
                                    <div class="row clearfix">
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <input type="checkbox" name="remember_me_1" id="remember_me_1" value="1" class="filled-in">
                                            <label for="remember_me_1" id="cash">Cash Payment</label>
                                            <input type="number" name="cash_payment" id="cash_payment" class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5"  placeholder="Cash Payment" style="display: none">
                                        </div>
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <input type="checkbox" name="remember_me_2" id="remember_me_2" value="2" class="filled-in">
                                            <label for="remember_me_2">Bank Payment</label>
                                            <input type="number" name="bank_payment" id="bank_payment" class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5" placeholder="Bank Payment" style="display: none">
                                        </div>
                                        <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                            <input type="checkbox" name="remember_me_3" id="remember_me_3" value="3" class="filled-in">
                                            <label for="remember_me_3">Buy The Remaining</label>
                                            <input type="number" name="buy_remaining" id="buy_remaining" class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5" placeholder="Buy The Remaining" style="display: none">
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align: right">Total Paid Amount: </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        {{--</form>--}}
                        {{--{{ Form::bsSubmit('Submit') }}--}}
                        <button type="submit" class="btn btn-primary" id="purchaseSubmit">Purchase Add</button>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    {{--payment type--}}

    {{--partho vai--}}
<script type="text/javascript">
    // journal entry validation
    $(document).ready(function() {
         var cash_payment = 0;
         var bank_payment = 0;
         var buy_remaining = 0;

        $("#purchaseSubmit").click(function(){
            var invoice_no = $("#invoice_no").val();
            var supplier = document.getElementById("supplier_name");
            var notes = $("#notes").val();
            var purchase_date = $("#purchase_date").val();

            var total_price = $("#total_price").val();

            var cash_payment = $("#cash_payment").val();
            var bank_payment = $("#bank_payment").val();
            var buy_remaining = $("#buy_remaining").val();

            var cheackValue = +cash_payment + +bank_payment + +buy_remaining;

            if(invoice_no==''){
                swal('Invoice No Empty');
                return false;
            }
            if(supplier.value=="supplier"){
                swal('Select Supplier');
                return false;
            }
            if(notes==''){
                swal('Notes No Empty');
                return false;
            }
            if(purchase_date==''){
                swal('purchase Date No Empty');
                return false;
            }
            if(total_price!=cheackValue){
                swal('Total price and input price are not equal');
                return false;
            }
        });
    });
</script>




    <script type="text/javascript">
        $(function () {
            $("#remember_me_1").click(function () {
                if ($(this).is(":checked")) {
                    $("#cash_payment").show();
                } else {
                    $("#cash_payment").trigger('reset');
                    $("#cash_payment").hide();
                }
            });
            $("#remember_me_2").click(function () {
                if ($(this).is(":checked")) {
                    $("#bank_payment").show();
                } else {
                    $("#bank_payment").hide();
                    $("#bank_payment").trigger('reset');
                }
            });
            $("#remember_me_3").click(function () {
                if ($(this).is(":checked")) {
                    $("#buy_remaining").show();
                } else {
                    $("#buy_remaining").hide();
                    $("#buy_remaining").trigger('reset');
                }
            });
        });
    </script>



    <script>
        $(function(){
            $(".cal_val").on("change paste keyup", function() {
                $(".ap_total").val("");
                var purchase_price = $("input[name='modal_purchase_price']").val();
                var discount_amount = $("input[name='modal_discount_amount']").val();
                var discount_percentage = $("input[name='modal_discount_percentage']").val();
                if(discount_amount>0){
                    var t = purchase_price - discount_amount;
                }else {
                    var total_dicount = (purchase_price * discount_percentage)/100;
                    var t = purchase_price - total_dicount;
                }
                $(".ap_total").val(function() {
                    return this.value + t;
                })
            })
        })
    </script>
    <script type="text/javascript">
        $( document ).ready(function() {
            var i = 0;
            var totalQuantity = 0;
            var totalPrice = 0;
            $("#id1").click(function(){
                i++;
             var x1 = document.getElementById("modal_medicine").value;
            var x2 = document.getElementById("modal_quantity").value;
            var x3 = document.getElementById("price").value;
                totalQuantity = Number(totalQuantity) + Number($("#modal_quantity").val());
                totalPrice = Number(totalPrice) + Number($("#price").val());
                document.getElementById('total_quantity').value=totalQuantity;
                document.getElementById('total_price').value=totalPrice;
            var rows = "";
            rows += '<tr><td><input type="text" class="form-control" name="rows['+i+'][modal_medicine_name]" value=" '+ x1 +' " id="modal_medicine_name"></td><td><input type="text" class="form-control" name="rows['+i+'][modal_medicine_quantity]" value="'+ x2 +' " id="modal_medicine_quantity"></td><td><input type="text" class="form-control" name="rows['+i+'][modal_discount_price]" value=" '+ x3 +' " id="modal_discount_price"></td><td><input type="button" value="Delete" id="addTblRow'+i+'"  onclick="deleteRow(this)"></td></tr>';
            $(rows).appendTo("#addTblRow");
            $("#modalFormReset").trigger('reset');
            });
        });
    </script>

    <script type="text/javascript">
        function showDiv(select){
            if(select.value==1){
                document.getElementById('discount_amount').style.display = "block";
                document.getElementById('discount_percentage').style.display = "none";
            }
            if(select.value==2){
                document.getElementById('discount_percentage').style.display = "block";
                document.getElementById('discount_amount').style.display = "none";
            }
        }


        function showDivModal(elem){
            if(elem.value == 1){
                document.getElementById('modal_discount_amount').style.display = "block";
                document.getElementById('modal_discount_percentage').style.display = "none";
            }
            if(elem.value == 2){
                document.getElementById('modal_discount_percentage').style.display = "block";
                document.getElementById('modal_discount_amount').style.display = "none";
            }
        }
    </script>
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function(){--}}
            {{--$("#journal-complete-entry").click(function(){--}}
                {{--$.ajaxSetup({--}}
                    {{--headers: {--}}
                        {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                    {{--}--}}
                {{--})--}}
                {{--var formData = {--}}
                    {{--journal_no: $('#journal_no').val(),--}}
                    {{--journal_date: $('#journal_date').val(),--}}
                    {{--notes: $('#notes').val()--}}
                {{--}--}}
                {{--$.ajax({--}}
                    {{--type:'post',--}}
                    {{--url: '/journalComplete',--}}
                    {{--data: formData,--}}
                    {{--success: function (data) {--}}
                        {{--alert(data);--}}
                        {{--toastr.success('Successfully add!', {timeOut: 300});--}}
                    {{--},--}}
                    {{--error: function (data) {--}}
                        {{--console.log('Error:', data);--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection