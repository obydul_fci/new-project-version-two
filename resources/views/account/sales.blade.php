@extends('layouts.admin_master')
@section('main_content')
    <script>
        //journal debit data
        $( document ).ready(function() {
            var i=0;
            var theTotal = '';
            $("#sales_save_id").click(function(){
                i++;
                var x1 = document.getElementById("medicine_id").value;
                var medicine=document.getElementById("medicine_id");
                var x2 = document.getElementById("sales_price").value;
                var x3 = document.getElementById("sales_quantity").value;
                var total_price=x2*x3;
                theTotal = Number(theTotal) + Number(total_price);
                if(medicine.value == "medicine_select") {
                    swal("Select Medicine");
                    return false;
                }
                else if(x2==''){
                    swal('Enter Price');
                    return false;
                }
                else if(x3==''){
                   swal('Enter Quantity');
                   return false;
                }
                else{
                    var rows3="";
                    rows3+='<tr id=""><td><input id="medicine_name_id" type="text" name="rows['+i+'][medicine_name]" class="form-control" value="'+x1+'"></td><td><input id="sales_price_id" type="text" name="rows['+i+'][sales_price_id]" class="form-control" value="'+x2+'"></td><td><input id="sales_quantity" type="text" name="rows['+i+'][sales_quantity]" class="form-control" value="'+x3+'"></td><td><input id="sales_total_amount" type="text" name="rows['+i+'][sales_total_amount]" class="form-control debit_amm" value="'+total_price+'"></td><td><button type="button" onclick="del_tr(this);return false" id="remove_sale_list" class="btn btn-danger">Remove</button></td></tr>';
                    $(rows3).appendTo("#sales_list_id");
                }
                document.getElementById('sale_sum_amount_id').value=theTotal;
            });

            $("#sale_complete").click(function() {
                var num = Math.floor(Math.random() * 90000) + 10000;
                var sale_no=$("#sales_no").val();
                var customer=document.getElementById("customer_id");
                var medicine=document.getElementById("medicine_id");
                var x2 = document.getElementById("sales_price").value;
                var x3 = document.getElementById("sales_quantity").value;
                document.getElementById('sales_no').value=num;
//                if(sale_no=='') {
//                    swal("Enter Sales No");
//                    return false;
//                }
                if(customer.value == "customer_select") {
                    swal("Select Customer");
                    return false;
                }
                if($("#date").val() == "") {
                    swal("Select Date");
                    return false;
                }
                if(medicine.value == "medicine_select") {
                    swal("Select Medicine");
                    return false;
                }
                if(x2==''){
                    swal('Enter Price');
                    return false;
                }
                if(x3==''){
                    swal('Enter Quantity');
                    return false;
                }
            });
        });
        function del_tr(remtr)
        {
            while((remtr.nodeName.toLowerCase())!='tr')
                remtr = remtr.parentNode;

            remtr.parentNode.removeChild(remtr);
        }
        function del_id(id)
        {
            del_tr(document.getElementById(id));
        }
    </script>
    <script>
        function showDivModal(elem){
            if(elem.value == 1){
                document.getElementById('discount_amount').style.display = "block";
                document.getElementById('discount_amount_btn').style.display = "block";
                document.getElementById('discount_percentage').style.display = "none";
                document.getElementById('discount_percent_btn').style.display = "none";
            }
            if(elem.value == 2){
                document.getElementById('discount_percentage').style.display = "block";
                document.getElementById('discount_percent_btn').style.display = "block";
                document.getElementById('discount_amount').style.display = "none";
                document.getElementById('discount_amount_btn').style.display = "none";
            }
        }
        $( document ).ready(function() {
            $("#discount_amount_btn").click(function(){
                var discount_amount=$("#discount_amount").val();
                var sale_sum_amount_id=$("#sale_sum_amount_id").val();
                var discount_amount_minus=sale_sum_amount_id-discount_amount;
                document.getElementById('discount_hidden_id').value=discount_amount;
                document.getElementById('sale_sum_amount_id').value=discount_amount_minus;
             });

            $("#discount_percent_btn").click(function(){
                var discount_percentage=$("#discount_percentage").val();
                var sale_sum_amount_id=$("#sale_sum_amount_id").val();
                var total_discount_percen=sale_sum_amount_id*discount_percentage/100;
                document.getElementById('discount_hidden_id').value=discount_percentage;
                document.getElementById('sale_sum_amount_id').value=total_discount_percen;
            });

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="medicine_id"]').on('change', function() {
                var medicineID = $(this).val();
                    $.ajax({
                        url: '/sales_medicine_price/'+medicineID,
                        type: 'GET',
                        success:function(data) {
                            $.each(data, function(key, value) {
                                var pur_price=value.purchase_price;
                                document.getElementById('sales_price').value=pur_price;
                            });
                        }
                    });
                });
          });
    </script>
    {!! Form::open(['url' =>'sales_store','class' => 'form_advanced_validation','method' => 'POST']) !!}
    <div class="well">
        <h4>SALES ENTRY</h4>
        <br>
        <div class="com-md-12">
            <div class="row clearfix">
                <div class="col-md-4 col-sm-4">
                    <label>Sales No</label>
                    <input type="text" class="form-control" name="sales_no" id="sales_no" placeholder="Sales No" value="">
                </div>
                <div class="col-md-4 col-sm-4">
                    <label>Select Customer <span style="color:red">*</span></label>
                    <select name="customer_id" id="customer_id" class="form-control">
                        <option value="customer_select">Select Customer</option>
                        @foreach($customer as $users)
                        <option value="{{$users->Customer_id}}">{{$users->Customer_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4 col-sm-4">
                        <label class="control-label" for="date">Sales Date <span style="color:red">*</span></label>
                        <input class="form-control myclass" id="date" name="date" placeholder="DD/MM/YYY" type="text"/>
                </div>
                <br><br><br><br>
                <div class="col-md-12 col-sm-12">
                    <label for="comment">Note:</label>
                    <textarea class="form-control" name="sale_notes" rows="5" id="sale_notes"></textarea>
                    <br><br>
                </div>
            </div>
            <input type="hidden" name="user_type" id="user_type" value="{{Sentinel::getUser()->full_name}}">
        </div>
    </div>
    <div class="well">
        <div class="row clearfix">
            <div class="col-md-4 col-sm-4">
                <label>Select Medicine <span style="color:red">*</span></label>
                <select name="medicine_id" id="medicine_id" class="form-control">
                    <option value="medicine_select">Select Medicine</option>
                    @foreach($medicine as $item)
                      <option value="{{ $item->id }}">{{$item->medicine_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4 col-sm-4">
                <label>Price<span style="color:red">*</span></label>
                <input type="text" class="form-control" name="sales_price" id="sales_price" placeholder="Price">
            </div>
            <div class="col-md-4 col-sm-4">
                <label>Quantity<span style="color:red">*</span></label>
                <input type="text" class="form-control" name="sales_quantity" id="sales_quantity" placeholder="Quantity">
            </div>
            <br><br><br><br>
            <div class="col-md-4 col-sm-4">
              <button type="button" id="sales_save_id" class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="body">
            <div class="table-responsive">
                <h5>Sales List</h5>
                <table id="sales_table" class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Medicine</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="sales_list_id" name="coa-list">
                    </tbody>
                </table>
                <div class="col-md-8 col-sm-8">
                  <b>Total Amount:</b><input class="ap_total" style="border: 0px solid #ffffff;" type="text" name="sale_sum_amount_price" id="sale_sum_amount_id">
                </div>
                <div class="col-md-4">
                    <label>Select Discount Type</label>
                    <select  name="modal_discount" id="modal_discount" class="form-control" onchange="showDivModal(this)">
                        <option value="0">Select Discount</option>
                        <option value="1">Amount</option>
                        <option value="2">Percentage</option>
                    </select>
                    <div class="col-md-12">
                        <input type="number" class="form-control cal_val" name="discount_amount" id="discount_amount" placeholder="Discount amount" style="display: none">
                        <button type="button" style="display: none;" id="discount_amount_btn" class="btn btn-warning">Save</button>
                    </div>
                    <input type="number" class="form-control cal_val" name="discount_percentage" id="discount_percentage" placeholder="Discount percentage" style="display: none">
                    <button type="button" style="display: none;" class="btn btn-warning" id="discount_percent_btn">Save</button>
                    <input type="hidden" id="real_discount_amount" name="discount">
                    <input type="hidden" name="discount" id="discount_hidden_id">
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="sale_complete">COMPLETE</button>
        </div>
    </div>
    {!! Form::close() !!}
@endsection