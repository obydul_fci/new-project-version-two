@extends('layouts.admin_master')
@section('main_content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Generic List
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        {{--<a href="{{url('roles/create')}}" class="dropdown-toggle btn btn-info">New Add</a>--}}
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#genericAdd"><i class="material-icons">add_box</i>Add New</button>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($genericsList as $generic)
                                <tr>
                                    <td>{{$generic->generic_name}}</td>
                                    <td>

                                        <button class="btn btn-primary waves-effect" data-toggle="modal" data-target="#genericEdit{{$generic->id}}" style="margin-left: 5px"> Edit</button>
                                        {{ Form::open(array('url' => 'genericsDelete/' . $generic->id, 'class' => 'pull-left')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                <!-- edit generic name -->
                                <!-- Modal -->
                                <div class="modal fade" id="genericEdit{{$generic->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Generic Edit form </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['url' =>['genericsUpdate',$generic->id],'class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                                                {{ Form::bsText('generic_name',$generic->generic_name) }}
                                                {{ Form::bsSubmit('Submit') }}
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- edit generic name end -->
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
    <!-- Add Generic Modal -->
    <div class="modal fade" id="genericAdd" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Generic Add</h4>
                </div>
                <div class="modal-body">
                    <p>Please select your input method.</p>
                    <div class="form-group">
                        <input type="radio" name="gender" id="male" class="with-gap">
                        <label for="male">Input</label>

                        <input type="radio" name="gender" id="female" class="with-gap">
                        <label for="female" class="m-l-20">Upload</label>
                    </div>
                    <div class="form-group" id="inputForm" hidden>
                        {!! Form::open(['url' =>'genericsAdd','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                        {{ Form::bsText('generic_name') }}
                        {{ Form::bsSubmit('Submit') }}
                        {!! Form::close() !!}
                    </div>
                    <div class="form-group" id="inputFile" hidden>
                        {!! Form::open(['url' =>'genericsFileAdd','class' => 'form_advanced_validation','method' => 'POST','files' => true]) !!}
                        <div class="form-line focused">
                            <input type="file" class="form-control" name="generic_name" required="" aria-required="true" aria-invalid="false" >
                            <label class="form-label">Select File</label>
                        </div><div class="help-info">upload only csv file</div><br/>

                        {{ Form::bsSubmit('Submit') }}
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function(){
            $("#male").click(function(){
                $("#inputForm").show();
                $("#inputFile").hide();
            });
            $("#female").click(function(){
                $("#inputFile").show();
                $("#inputForm").hide();
            });
        });
    </script>
@endsection