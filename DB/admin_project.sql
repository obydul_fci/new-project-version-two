-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2017 at 01:45 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'hCIV5FE9PVOUeXwAdgWM57OZvuZWD0a1', 1, '2017-11-29 22:27:50', '2017-11-29 22:27:50', '2017-11-29 22:27:50'),
(2, 2, '6eogekOfIZBTf12USJZsLZ07hLNEWfct', 1, '2017-11-30 00:45:25', '2017-11-30 00:45:25', '2017-11-30 00:45:25'),
(3, 3, 'nuXVMLz7Mi47C5NwOSAsd1WLVE4ZwXbK', 1, '2017-11-30 00:45:54', '2017-11-30 00:45:54', '2017-11-30 00:45:54'),
(4, 4, 'FpRUtg3MpwAA34r3sYjR93Jwt02nX8Hb', 1, '2017-12-01 23:01:03', '2017-12-01 23:01:02', '2017-12-01 23:01:03'),
(5, 5, 'afJWNsgcQFe1g2JOf6RZSFSlXCdfVO1f', 1, '2017-12-02 01:23:17', '2017-12-02 01:23:17', '2017-12-02 01:23:17');

-- --------------------------------------------------------

--
-- Table structure for table `media_upload`
--

CREATE TABLE `media_upload` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media_upload`
--

INSERT INTO `media_upload` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, 'sad-child-portrait.jpg', '2017-12-05 03:14:31', '2017-12-05 03:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_12_02_070647_add_gender_users_table', 2),
(4, '2017_12_04_043845_create_user_loghistory_table', 3),
(5, '2017_12_04_052248_create_user_loghistory_table', 4),
(6, '2017_12_05_043507_add_softdelete_to_users_history_table', 5),
(7, '2017_12_05_084734_create_medita_file_table', 6),
(8, '2017_12_05_114050_user_permissions', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'Display Role Listing', 'See only Listing Of Role', '2017-12-05 05:56:01', '2017-12-05 05:56:01'),
(2, 'role-create', 'Create Role', 'Create New Role', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(3, 'role-edit', 'Edit Role', 'Edit Role', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(4, 'role-delete', 'Delete Role', 'Delete Role', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(5, 'item-list', 'Display Item Listing', 'See only Listing Of Item', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(6, 'item-create', 'Create Item', 'Create New Item', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(7, 'item-edit', 'Edit Item', 'Edit Item', '2017-12-05 05:56:02', '2017-12-05 05:56:02'),
(8, 'item-delete', 'Delete Item', 'Delete Item two', '2017-12-05 05:56:02', '2017-12-05 05:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `user_id`, `permission_id`, `role_id`) VALUES
(1, 1, 2, 2),
(2, 5, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(2, 1, 'KFJq1GnU8JLA0t8ntZUlIb5lcmcesGq5', '2017-11-29 22:59:09', '2017-11-29 22:59:09'),
(4, 1, 'oSHHxC49CqPDKA7TeJJHRj65NFoUKW0v', '2017-11-30 00:46:05', '2017-11-30 00:46:05'),
(6, 3, '6TVh1fQCU2AwktXOnhRrn089sOit6tmO', '2017-11-30 05:01:08', '2017-11-30 05:01:08'),
(7, 1, 'Vh2hEPYfk2pezBMm44u3ButAxYWvNoFW', '2017-12-02 02:50:00', '2017-12-02 02:50:00'),
(8, 1, 'opmEJDcQL9S6Q4nXGhSbb3dcEjXZfbki', '2017-12-02 23:54:24', '2017-12-02 23:54:24'),
(9, 1, '4OkpkMJDKpuYihmsuYVVcR9cRJtjj1qW', '2017-12-03 23:01:49', '2017-12-03 23:01:49'),
(13, 1, 'rHlhJzooDUb1HLpmiwgP9lBunQToXxdU', '2017-12-04 00:49:58', '2017-12-04 00:49:58'),
(14, 1, 'WsoRKxpu4zSjAlywjV2afJUZCsNdOEQG', '2017-12-04 00:51:17', '2017-12-04 00:51:17'),
(15, 1, 'BwAWbdq6gKQmC2xLInxXUEHxggNuN2CA', '2017-12-04 00:52:07', '2017-12-04 00:52:07'),
(16, 1, 'IqIUZs7UMQwITotrHKig8hIDlbKdmZgY', '2017-12-04 00:59:21', '2017-12-04 00:59:21'),
(17, 1, 'YBOd2MlOqIHKQS8X0slfNEoziP5H54xS', '2017-12-04 01:11:03', '2017-12-04 01:11:03'),
(21, 1, 'GsitjDfNcqKJpgktgi0MxpDUsUrrHU43', '2017-12-04 01:18:09', '2017-12-04 01:18:09'),
(28, 1, 'ebWd9Y1EicKjmejjEfkwp8N69JbCzdbB', '2017-12-04 09:16:46', '2017-12-04 09:16:46'),
(31, 1, 'gu66i7LCNvqWSVLqscS3sx3DYHxfT3Kc', '2017-12-04 11:54:32', '2017-12-04 11:54:32'),
(35, 1, 'xYAniY3TjjG2qHV00FMLsEZMjukoKecg', '2017-12-05 08:21:39', '2017-12-05 08:21:39');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(2, 'supper_admin', 'Supper Admin', 'Supper admin user', '2017-11-30 00:35:24', '2017-11-30 00:39:57'),
(4, 'user', 'User', 'Generall user', '2017-11-30 00:44:14', '2017-11-30 00:44:14'),
(5, 'admin', 'Admin', 'Admin user', '2017-11-30 00:44:40', '2017-11-30 00:44:40'),
(6, 'koko', 'olllll', 'oooo', '2017-12-04 04:22:16', '2017-12-04 04:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2017-11-29 22:27:50', '2017-11-29 22:27:50'),
(2, 2, 4, '2017-11-30 00:45:25', '2017-11-30 00:45:25'),
(3, 3, 5, '2017-11-30 00:45:54', '2017-11-30 01:24:11'),
(4, 1, 5, '2017-11-30 01:16:46', '2017-11-30 01:16:46'),
(5, 1, 4, '2017-11-30 01:21:15', '2017-11-30 01:21:15'),
(6, 4, 4, '2017-12-01 23:01:03', '2017-12-01 23:01:03'),
(7, 5, 4, '2017-12-02 01:23:17', '2017-12-02 01:23:17'),
(8, 6, 4, '2017-12-02 02:05:03', '2017-12-02 02:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profession` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `division` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upzila` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `full_name`, `address`, `picture`, `mobile`, `phone`, `gender`, `birth_date`, `profession`, `division`, `district`, `upzila`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', '$2y$10$yJPnvzrZZVM9Nwwt3pNBme38ODj3xqw.jNmi33bpmY70otkYBiVhm', NULL, '2017-12-05 08:21:39', 'obydul', '', '', '', '', '1', '', '', '', '', '', '2017-11-29 22:27:50', '2017-12-05 08:21:39'),
(2, 'user@gmail.com', '$2y$10$PqPT4H1/OViqyLdxYF.foOL/VDA.BbSaza0T1PqsFjBRa62OuZ5F6', NULL, '2017-12-05 07:05:32', 'user', '', '', '', '', '', '', '', '', '', '', '2017-11-30 00:45:25', '2017-12-05 07:05:32'),
(3, 'adminuser@gmail.com', '$2y$10$/hlgDoJst5jL8TNwIADDLuDQewO2rKjKeNFNiJxaoer/I0ex03Sfu', NULL, '2017-12-01 22:55:02', 'admin', '', '', '', '', '', '', '', '', '', '', '2017-11-30 00:45:54', '2017-12-01 22:55:02'),
(4, 'obydulhaque960@gmail.com', '$2y$10$sgmXAzvsQSjvXgJxDwpatOoqoqUlgPL5KlTwvR/Dn9nWCnd4XQ8Py', NULL, NULL, 'kamrul', '', '', '', '', '', '', '', '', '', '', '2017-12-01 23:01:02', '2017-12-01 23:01:02'),
(5, 'partho@gmil.com', '$2y$10$SU2DoInladqdB7JQ7W1Rf.xUqbC53vGpyvIbncnryzGNudeeuwep2', NULL, NULL, 'partho', '', '', '', '', '1', '', '', '', '', '', '2017-12-02 01:23:17', '2017-12-02 01:23:17'),
(6, 'ob@gmail.com', '$2y$10$.vOp2NDx86U.Z.j057VZwuY1KxULCppY8AZcok4EvaHws/2wSSpby', NULL, NULL, 'obydul haque', NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, '2017-12-02 02:05:03', '2017-12-02 02:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_loghistory`
--

CREATE TABLE `user_loghistory` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_device` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser_version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_page_view` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logout_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_activity` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'logout',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_loghistory`
--

INSERT INTO `user_loghistory` (`id`, `user_id`, `user_ip`, `user_device`, `browser_version`, `user_page_view`, `user_location`, `login_time`, `logout_time`, `last_activity`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '2017-12-15 00:00:00', '01:06:18 PM', NULL, 'Logout', '2017-12-03 23:23:36', '2017-12-04 23:27:50', '2017-12-04 23:27:50'),
(5, '1', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '2017-12-04 07:18:49', '01:06:18 PM', NULL, 'Logout', '2017-12-04 01:18:49', '2017-12-04 01:18:49', NULL),
(6, '1', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '2017-12-04 07:21:20am', '01:06:18 PM', NULL, 'Logout', '2017-12-04 01:21:20', '2017-12-04 01:21:20', NULL),
(8, '1', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '2017-12-04 01:37:14 PM', '01:06:18 PM', NULL, 'Logout', '2017-12-04 07:37:14', '2017-12-04 07:37:14', NULL),
(9, '1', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '01:38:34 PM', '01:06:18 PM', NULL, 'Logout', '2017-12-04 07:38:34', '2017-12-04 07:38:34', NULL),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '02:33:38 PM', NULL, 'logout', '2017-12-04 08:33:38', '2017-12-04 08:33:38', NULL),
(13, '1', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '04:05:23 PM', '01:06:18 PM', NULL, 'Logout', '2017-12-04 10:05:23', '2017-12-04 23:09:33', '2017-12-04 23:09:33'),
(14, '2', '127.0.0.1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', '127.0.0.1', '127.0.0.1', '04:29:50 PM', NULL, NULL, 'Login...', '2017-12-04 10:29:50', '2017-12-04 10:29:50', NULL),
(15, '1', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '05:54:32 PM', '01:06:18 PM', NULL, 'Logout', '2017-12-04 11:54:32', '2017-12-04 11:54:32', NULL),
(16, '1', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '10:01:48 AM', '01:06:18 PM', NULL, 'Logout', '2017-12-05 04:01:48', '2017-12-05 04:01:48', NULL),
(17, '1', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '01:04:22 PM', '01:06:18 PM', NULL, 'Logout', '2017-12-05 07:04:22', '2017-12-05 07:04:22', NULL),
(18, '2', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '01:05:32 PM', NULL, NULL, 'Login...', '2017-12-05 07:05:32', '2017-12-05 07:05:32', NULL),
(19, '1', '127.0.0.1', NULL, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', NULL, NULL, '02:21:39 PM', NULL, NULL, 'Login...', '2017-12-05 08:21:39', '2017-12-05 08:21:39', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_upload`
--
ALTER TABLE `media_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_loghistory`
--
ALTER TABLE `user_loghistory`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `media_upload`
--
ALTER TABLE `media_upload`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role_users`
--
ALTER TABLE `role_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_loghistory`
--
ALTER TABLE `user_loghistory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
