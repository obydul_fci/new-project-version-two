
$(document).ready(function(){
    //get base URL *********************
    var url = $('#url').val();
    //display modal form for creating new product *********************
    $('#btn_add').click(function(){
        $('#btn-save').val("add");
        $('#frmProducts').trigger("reset");
        $('#myModal').modal('show');
    });
    //create new product / update existing product ***************************
    $("#btn-save").click(function (e) {
       //some validation check
        var mdicine_name_error=$('#medicine_name').val();
        var mdicine_company_error=$('#medicine_company').val();
        var medicine_error_type =  document.getElementById("medicine_type");
        var mdicine_quantity_error=$('#medicine_quantity').val();
        var mdicine_purchase_error=$('#purchase_price').val();
        var mdicine_sell_error=$('#sell_price').val();
        if(mdicine_name_error==''){
            swal("Please give a Medicine name!")
        }
        else if(mdicine_company_error==''){
            swal("Please give a Company name!")
        }
        else if (medicine_error_type.value == "SelectTypes") {
            swal("Please select a medicine type!")
        }
        else if(mdicine_quantity_error==''){
            swal("Please Enter box piece")
        }
        else if(mdicine_purchase_error==''){
            swal("Please Enter purchase price")
        }
        else if(mdicine_sell_error==''){
            swal("Please Enter sale price")
        }
        else{
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        e.preventDefault();
        var formData = {
            medicine_name: $('#medicine_name').val(),
            medicine_company: $('#medicine_company').val(),
            medicine_type: $('#medicine_type').val(),
            generic: $('#generic').val(),
            medicine_code: $('#medicine_code').val(),
            medicine_rack: $('#medicine_rack').val(),
            medicine_quantity: $('#medicine_quantity').val(),
            purchase_price: $('#purchase_price').val(),
            sell_price: $('#sell_price').val(),
            medicine_effect: $('#medicine_effect').val(),
            expire_date: $('#expire_date').val(),
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#btn-save').val();
        var type = "POST"; //for creating new resource
        var product_id = $('#product_id').val();;
        var my_url = url;
        if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/' + product_id;
        }
        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var product = '<tr id="product' + data.id + '">' + '<td>' + data.id + '</td> <td>' + data.medicine_name + '</td> <td>' + data.medicine_company + '</td> <td>' + data.medicine_type + '</td> <td>' + data.generic + '</td> <td>' + data.medicine_code + '</td> <td>' + data.medicine_rack + '</td> <td>' + data.medicine_quantity + '</td> <td>' + data.purchase_price + '</td> <td>' + data.sell_price + '</td> <td>' + data.medicine_effect + '</td> <td>' + data.expire_date + '</td> ';
                product += '<td><button class="btn btn-warning btn-detail open_modal" value="' + data.id + '">Edit</button>';
                product += ' <button class="btn btn-danger btn-delete delete-product" value="' + data.id + '">Delete</button></td></tr>';
                if (state == "add"){ //if user added a new record
                    $('#products-list').append(product);
                }else{ //if user updated an existing record
                    $("#product" + product_id).replaceWith( product );
                }
                $('#frmProducts').trigger("reset");
                $('#myModal').modal('hide')
                toastr.success('Successfully add!', {timeOut: 300});
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

        }
    });

    //display modal form for product EDIT ***************************
    $(document).on('click','.open_modal',function(){
        var product_id = $(this).val();
       
        // Populate Data in Edit Modal Form
        $.ajax({
            type: "GET",
            url: url + '/' + product_id,
            success: function (data) {
                console.log(data);
                $('#product_id').val(data.id);
                $('#medicine_name').val(data.medicine_name);
                $('#medicine_company').val(data.medicine_company);
                $('#medicine_type').val(data.medicine_type);
                $('#generic').val(data.generic);
                $('#medicine_code').val(data.medicine_code);
                $('#medicine_rack').val(data.medicine_rack);
                $('#medicine_quantity').val(data.medicine_quantity);
                $('#purchase_price').val(data.purchase_price);
                $('#sell_price').val(data.sell_price);
                $('#medicine_effect').val(data.medicine_effect);
                $('#expire_date').val(data.expire_date);
                $('#btn-save').val("update");
                $('#myModal').modal('show');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    //delete product and remove it from TABLE list ***************************
    $(document).on('click','.delete-product',function(){
        var product_id = $(this).val();
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + product_id,
            success: function (data) {
                console.log(data);
                $("#product" + product_id).remove();
               toastr.success('Successfully Deleted!', {timeOut: 300});
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});