
$(document).ready(function(){
    //get base URL *********************
    var url = $('#url').val();
    //display modal form for creating new journal *********************
    $('#btn_add').click(function(){
        $('#journal-master-save').val("add");
        $('#frmJournalMaster').trigger("reset");
        $('#journalMasterModal').modal('show');
    });
    //create new journal / update existing journal ***************************
    $("#journal-master-save").click(function (e) {
        //some validation check
        var journal_no_error=$('#journal_no').val();
        var journal_date_error=$('#journal_date').val();
        var journal_notes_error=$('#journal_notes').val();
        if(journal_no_error==''){
            swal("Please give a Journal No!")
        }
        else if(journal_date_error==''){
            swal("Please give a Journal Date!")
        }
        else if(journal_notes_error==''){
            swal("Please give a Journal Notes!")
        }
        else{
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            e.preventDefault();
            var formData = {
                journal_no: $('#journal_no').val(),
                journal_date: $('#journal_date').val(),
                journal_notes: $('#journal_notes').val(),
            }
            //used to determine the http verb to use [add=POST], [update=PUT]
            var state = $('#journal-master-save').val();
            var type = "POST"; //for creating new resource
            var journal_master_id = $('#journal_master_id').val();;
            var my_url = url;
            if (state == "update"){
                type = "PUT"; //for updating existing resource
                my_url += '/' + journal_master_id;
            }
            console.log(formData);
            $.ajax({
                type: type,
                url: my_url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var journalMaster = '<tr id="journalMaster' + data.id + '">' + '<td>' + data.id + '</td> <td>' + data.journal_no + '</td> <td>' + data.journal_date + '</td> <td>' + data.journal_notes + '</td> ';
                    journalMaster += '<td><button class="btn btn-warning btn-detail open_modal" value="' + data.id + '">Edit</button>';
                    journalMaster += ' <button class="btn btn-danger btn-delete delete-journalMaster" value="' + data.id + '">Delete</button></td></tr>';
                    if (state == "add"){ //if user added a new record
                        $('#journal-master-list').append(journalMaster);
                    }else{ //if user updated an existing record
                        $("#journalMaster" + journal_master_id).replaceWith( journalMaster );
                    }
                    $('#frmJournalMaster').trigger("reset");
                    $('#journalMasterModal').modal('hide')
                    toastr.success('Successfully add!', {timeOut: 300});
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        }
    });

    //display modal form for journal EDIT ***************************
    $(document).on('click','.open_modal',function(){
        var journal_master_id = $(this).val();

        // Populate Data in Edit Modal Form
        $.ajax({
            type: "GET",
            url: url + '/' + journal_master_id,
            success: function (data) {
                console.log(data);
                $('#journal_master_id').val(data.id);
                $('#journal_no').val(data.journal_no);
                $('#journal_notes').val(data.journal_notes);
                $('#journal-master-save').val("update");
                $('#journalMasterModal').modal('show');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    //delete journal and remove it from TABLE list ***************************
    $(document).on('click','.delete-journalMaster',function(){
        var journal_master_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + journal_master_id,
            success: function (data) {
                console.log(data);
                $("#journalMaster" + journal_master_id).remove();
                toastr.success('Successfully Deleted!', {timeOut: 300});
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});