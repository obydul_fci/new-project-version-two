﻿$(document).ready(function(){
    //get base URL *********************
    var coa_url = $('#coa-url').val();
    //display modal form for creating new product *********************
    $('#coa_add').click(function(){
        $('#coa-save').val("coa-add");
        $('#coa-Products').trigger("reset");
        $('#coa-Modal').modal('show');
        // var ParentData = {
        //     acc_name: $('#acc_name').val(),
        // }
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // })
        // $.ajax({
        //     type:'GET',
        //     url:'/account_parent',
        //     dataType: 'json',
        //     data:ParentData,
        //     success:function(data){
        //         var selectdata='';
        //         selectdata+='<option value="0">Select Parent</option>';
        //         for(i=0;i<data.length;i++){
        //             selectdata+='<option value="'+data[i]['id']+'">'+data[i]['parent']+' '+data[i]['acc_name']+' </option>';
        //         }
        //         console.log(selectdata);
        //         $('#parent').html(selectdata);
        //         // $('#select-input').selectpicker();
        //     }
        // });
    });
    //create new product / update existing product ***************************
    $("#coa-save").click(function (e) {
       //some validation check
        var acc_code=$('#acc_code').val();
        var acc_name=$('#acc_name').val();
        var acc_type =  document.getElementById("acc_type");
        var openning_balance=$('#openning_balance').val();
        var parent=$('#parent').val();
        var note=$('#note').val();
        if(acc_code==''){
            swal("Please give a account code!")
        }
        else if(acc_name==''){
            swal("Please give a account name!")
        }
        // else if (acc_type.value == "SelectTypes") {
        //     swal("Please select account type!")
        // }
        // else if(openning_balance==''){
        //     swal("Please fillup this field")
        // }
        // else if(note==''){
        //     swal("Please Enter some value")
        // }
        else{
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        e.preventDefault();
        var formData = {
            acc_code: $('#acc_code').val(),
            acc_name: $('#acc_name').val(),
            acc_type: $('#acc_type').val(),
            openning_balance: $('#openning_balance').val(),
            parent: $('#parent').val(),
            note: $('#note').val(),
            status: $('#status').val(),
            created_by: $('#created_by').val(),
            modified_by: $('#modified_by').val(),
        }
        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = $('#coa-save').val();
        var type = "POST"; //for creating new resource
        var coa_id = $('#coa_id').val();
        var my_url = coa_url;
        if (state == "update"){
            type = "PUT"; //for updating existing resource
            my_url += '/' + coa_id;
        }
        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                 console.log(data);
                 location.reload();
                    var coa = '<tr id="coa' + data.id + '">' + '<td>' + data.id + '</td> <td>' + data.acc_code + '</td> <td>' + data.acc_name + '</td> <td>' + data.acc_type + '</td> <td>' + data.openning_balance + '</td> <td>' + data.parent + '</td> <td>' + data.note + '</td> <td>' + data.status + '</td> <td>' + data.created_by + '</td> <td>' + data.modified_by + '</td> ';
                    coa += '<td><button class="btn btn-warning btn-detail chart_modal" value="' + data.id + '">Edit</button>';
                    coa += '<button class="btn btn-danger btn-delete delete-coa" value="' + data.id + '">Delete</button></td></tr>';
                if (state == "coa-add"){ //if user added a new record
                    $('#coa-list').append(coa);
                }else{ //if user updated an existing record
                    $("#coa" + coa_id).replaceWith( coa );
                }
                $('#coa-Products').trigger("reset");
                $('#coa-Modal').modal('hide');
                toastr.success('Successfully add!', {timeOut: 300});
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        }
    });

    //display modal form for product EDIT ***************************
    $(document).on('click','.chart_modal',function(){
        var coa_id = $(this).val();
        // Populate Data in Edit Modal Form
        $.ajax({
            type: "GET",
            url: coa_url + '/' + coa_id,
            success: function (data) {
                $('#coa_id').val(data.id);
                $('#acc_code').val(data.acc_code);
                $('#acc_name').val(data.acc_name);
                $('#acc_type').val(data.acc_type);
                $('#openning_balance').val(data.openning_balance);
                $('#parent').val(data.parent);
                $('#note').val(data.note);
                $('#created_by').val(data.created_by);
                $('#modified_by').val(data.modified_by);
                $('#coa-save').val("update");
                $('#coa-Modal').modal('show');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

//delete product and remove it from TABLE list ***************************
    $(document).on('click','.delete-coa',function(){
        var coa_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: coa_url + '/' + coa_id,
            success: function (data) {
                console.log(data);
                $("#coa" + coa_id).remove();
                toastr.success('Successfully Deleted!', {timeOut: 300});
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});