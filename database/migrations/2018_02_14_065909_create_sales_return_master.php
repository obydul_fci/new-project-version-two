<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReturnMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_return_no');
            $table->dateTime('sales_return_date');
            $table->integer('customer_id');
            $table->string('notes');
            $table->integer('created_by');
            $table->integer('modified_by');
            $table->timestamp('modified_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_return_masters');
    }
}
