<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Medicine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine', function (Blueprint $table) {
            $table->increments('id');
            $table->string('medicine_name');
            $table->string('medicine_company');
            $table->string('medicine_type');
            $table->string('medicine_image');
            $table->string('generic');
            $table->string('medicine_code');
            $table->string('medicine_rack');
            $table->integer('medicine_quantity');
            $table->decimal('purchase_price',10,2);
            $table->decimal('sell_price',10,2);
            $table->string('medicine_effect');
            $table->string('expire_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicine');
    }
}
