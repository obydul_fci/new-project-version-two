<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_no');
            $table->integer('customer_id');
            $table->decimal('discount',15,2)->nullable();
            $table->decimal('total_sum',15,2);
            $table->string('sales_date');
            $table->string('notes')->nullable();
            $table->decimal('discount',15,2);
            $table->string('sales_date');
            $table->string('notes');
            $table->string('created_by');
            $table->string('modified_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_master');
    }
}
