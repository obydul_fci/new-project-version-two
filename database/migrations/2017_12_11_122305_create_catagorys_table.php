<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatagorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug',50);
            $table->string('category_name');
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('subcategory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_category_id');
            $table->string('subcategory_name');
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sub_sub_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_category_id');
            $table->integer('subcategory_id');
            $table->string('sub_sub_category_name');
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_category');
        Schema::dropIfExists('subcategory');
        Schema::dropIfExists('sub_sub_category');
    }
}
