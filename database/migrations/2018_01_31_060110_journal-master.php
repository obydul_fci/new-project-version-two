<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JournalMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal-master', function (Blueprint $table) {
            $table->increments('id');
            $table->string('journal_no');
            $table->string('journal_date');
            $table->string('notes')->nullable();
            $table->tinyInteger('status')->nullable()->unsigned();
            $table->string('created_by');
            $table->string('modified_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journal-master');
    }
}
