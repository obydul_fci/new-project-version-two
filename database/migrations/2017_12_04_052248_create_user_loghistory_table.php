<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoghistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loghistory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->nullable();
            $table->string('user_ip')->nullable();
            $table->string('user_device')->nullable();
            $table->string('browser_version')->nullable();
            $table->string('user_page_view')->nullable();
            $table->string('user_location')->nullable();
            $table->string('login_time')->nullable();
            $table->string('logout_time')->nullable();
            $table->timestamp('last_activity')->nullable();
            $table->string('status')->default('logout');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_loghistory');
    }
}
