<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoaHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coa-head', function (Blueprint $table) {
            $table->increments('id');
            $table->string('acc_code');
            $table->string('acc_name');
            $table->string('acc_type');
            $table->decimal('openning_balance',15,2);
            $table->integer('parent');
            $table->string('note');
            $table->string('status');
            $table->string('created_by');
            $table->string('modified_by');
            $table->tinyInteger('is_delete')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coa-head');
    }
}
