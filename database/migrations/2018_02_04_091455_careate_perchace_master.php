<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareatePerchaceMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->string('invoice_no');
            $table->dateTime('purchase_date');
            $table->string('notes');
            $table->decimal('discount_amount',18,2);
            $table->integer('created_by');
            $table->integer('modified_by');
            $table->timestamp('modified_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_masters');
    }
}
