<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/','Admin\UserLoginController@create');
 Route::get('user_reg','Admin\UserRegistrationController@userRegForm');
 Route::post('user_regstore', 'Admin\UserRegistrationController@userRegStore');
 // add user with admin
 Route::post('add_user', 'Admin\UserRegistrationController@addUser');
 //user login
 Route::get('user_login', 'Admin\UserLoginController@create');
 Route::post('user_login', 'Admin\UserLoginController@userLogin');
 //facebook login
 //Route::get('auth/facebook', 'Admin\UserLoginController@redirectToFacebook');
 //Route::get('auth/facebook/callback', 'Admin\UserLoginController@handleFacebookCallback');
 // Linkdin login
 //Route::get('auth/linkedin', 'Admin\UserLoginController@redirectToLinkedin');
 //Route::get('auth/linkedin/callback', 'Admin\UserLoginController@handleLinkedinCallback');
 Route::get('user_logout', 'Admin\UserLoginController@userLogOut');

 Route::get('dashboard', function () {
    return view('welcome');

});

 /* User profile route */
 Route::get('profile', 'Admin\UserProfileController@index');
 Route::get('profile/update', 'Admin\UserProfileController@show');
 Route::post('profile', 'Admin\UserProfileController@update');

 Route::group(array('middleware' => ['admin']), function () {

 //clear cache from route, view, config and all cache data from application
  Route::get('cachClear', function () {
    /* Reoptimized class loader : php artisan optimize */
  \Artisan::call('optimize');
     /* Clear Cache facade value : php artisan cache:clear */
  \Artisan::call('cache:clear');
     /* Clear Route cache : php artisan route:clear */
  \Artisan::call('route:clear');
      /* Clear View cache :php artisan view:clear */
  \Artisan::call('view:clear');
      /* Clear Config cache : php artisan config:clear */
  \Artisan::call('config:clear');
      return Redirect::back();

    });

 //user Login activity show and delete
    Route::get('all_user_log_history', 'Admin\UserLogHistoryController@index');
    Route::get('single_user_log_history/{id}', 'Admin\UserLogHistoryController@singleUserHistory');
    Route::get('permanently_delete/{id}', 'Admin\UserLogHistoryController@permanentlyDelete');

    //user role create,edit,delete,show
    Route::resource('roles', 'Admin\RoleController');
    Route::resource('roleAssigns', 'Admin\RoleAssiginController');
    //user permissions create,edit,delete, show, assing
    Route::resource('permissions', 'Admin\PermissionController');
    Route::resource('permissionAssigns', 'Admin\PermissionAssiginController');


 //Display Medicine Index Page
    Route::get('/medicine', 'Admin\MedicineController@index');
    Route::get('medicine/{product_id?}', 'Admin\MedicineController@show');
    Route::post('medicine', 'Admin\MedicineController@store');
    Route::put('medicine/{product_id}', 'Admin\MedicineController@update');
    Route::delete('medicine/{product_id}', 'Admin\MedicineController@destroy');


 //create New Chart of Account head
//Route::get('/account', 'Admin\ChartAccountHead@index');
    Route::get('/account', 'Admin\ChartAccountHead@index');
    Route::get('/account_parent', 'Admin\ChartAccountHead@view_parent');

 //store chart account data
    Route::post('account', 'Admin\ChartAccountHead@store');
    Route::get('account/{coa_id?}', 'Admin\ChartAccountHead@show');
    Route::put('account/{coa_id}', 'Admin\ChartAccountHead@update');
    Route::delete('account/{coa_id}', 'Admin\ChartAccountHead@destroy');
    Route::get('/testdata','Admin\ChartAccountHead@just_test');

 //create journal entry
    Route::get('/journal','Admin\JournalController@index');
    Route::post('journal','Admin\JournalController@store');

 //Sales Entry
    Route::get('sales','Admin\sales@index');
    Route::post('sales_store','Admin\sales@store');
    Route::get('sales_medicine_price/{id}','Admin\sales@show');

  // generic List
    Route::get('generics','Admin\GenericsController@index');
    Route::post('genericsAdd', 'Admin\GenericsController@store');
    Route::post('genericsFileAdd', 'Admin\GenericsController@fileStore');
    Route::post('genericsUpdate/{id}', 'Admin\GenericsController@update');
    Route::delete('genericsDelete/{id}', 'Admin\GenericsController@destroy');

  //media library
    Route::resource('media_file', 'Admin\MediaUploadController');
    Route::resource('user_activation', 'Admin\UserActivationController');
    Route::get('show_user/{id}','Admin\UserActivationController@edit');

   // categorys settings

    Route::resource('categorys', 'Admin\CategoryController');
    Route::post('categorys_update/{id}', 'Admin\CategoryController@main_category_activation');

   //subcategory settings

    Route::get('subcategorys', 'Admin\CategoryController@subcategory');
    Route::delete('subcategorys_delete/{id}', 'Admin\CategoryController@sub_category_delete');



     // purchase
     Route::get('/purchase', 'Admin\PurchaseController@index');
     Route::post('/purchaseStore', 'Admin\PurchaseController@store');
     // sales return
     Route::get('/salesReturn', 'Admin\SalesReturnController@index');
     Route::get('/salesInvoiceWiseMedicine', 'Admin\SalesReturnController@invoiceWiseMedicine');
     Route::get('/invoiceWiseMedicineDetails', 'Admin\SalesReturnController@invoiceWiseMedicineDetails');
     Route::post('/salesReturnStore', 'Admin\SalesReturnController@store');

     // report
     Route::get('/purchasesReport', 'Admin\ReportController@index');




});

Route::group(array('middleware' => ['admin2']), function () {
    Route::get('form', function () {
        return view('example.form');
    });
});

Route::group(array('middleware' => ['user']), function () {
    Route::get('table', function () {
        return view('example.table');
    });
});