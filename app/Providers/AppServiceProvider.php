<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\FooterSettings;
use App\Models\HeaderSettings;
use App\Models\User;
use Sentinel;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // database default string length
      Schema::defaultStringLength(191);

      // Count new register user
      $count = User::where('status','=',0)->count();
      view()->share('count', $count);

      // show user after count
        $user_list_notification = User::where('status', 0)
            ->orderBy('id', 'desc')->get();
        view()->share('user_list_notification', $user_list_notification);


        //Total user Count
        $total_user_count=User::all()->count();
        view()->share('total_user_count',$total_user_count);


        $test = Sentinel::findById(1);
        view()->share('test', $test);
      // user access condition
        $userID = Sentinel::findById(1);
        $userPermissionAccess = DB::table('users')->where('users.id',$userID->id)
            ->join('role_users','users.id','=','role_users.user_id')
            ->join('roles','role_users.role_id','=','roles.id')
            ->select('users.id','users.full_name','roles.name','roles.permissions')->first();
        view()->share('userPermissionAccess', $userPermissionAccess);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
