<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Generics extends Model
{
    protected $table = 'generics';
    protected $fillable = ['id','generic_name'];
}
