<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class journal_details extends Model
{
    protected $table='journal-details';
    protected $fillable = ['journal_id','coa_id','debit','credit','created_at',''];
    public $timestamps = true;
}
