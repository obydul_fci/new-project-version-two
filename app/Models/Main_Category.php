<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Main_Category extends Model
{
    use SoftDeletes;
    protected $table = 'main_category';
    protected $fillable = ['id','category_name','status'];
}
