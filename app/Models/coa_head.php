<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class coa_head extends Model
{
    protected $table='coa-head';
    protected $fillable = ['acc_code','acc_name','acc_type','openning_balance','parent','note','status','is_delete','created_by','modified_by'];
}
