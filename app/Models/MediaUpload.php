<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaUpload extends Model
{
    protected $table = 'media_upload';
    protected $primaryKey = "id";
    protected $fillable = ['image'];
}
