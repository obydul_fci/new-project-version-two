<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class journal_master extends Model
{
    protected $table='journal-master';
    protected $fillable = ['journal_no','journal_date','notes'];
    protected $timestamps = true;
}
