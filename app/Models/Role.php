<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sentinel;
use DB;
class Role extends Model
{
    //
public function userAccessShare(){
    $userID = Sentinel::findById(1);
    $userPermissionAccess = DB::table('users')->where('users.id',$userID->id)
        ->join('role_users','users.id','=','role_users.user_id')
        ->join('roles','role_users.role_id','=','roles.id')
        ->select('users.id','users.full_name','roles.name','roles.permissions')->first();
    return $userPermissionAccess;
}
}
