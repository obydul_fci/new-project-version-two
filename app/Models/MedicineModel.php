<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicineModel extends Model
{
    protected $table = 'medicine';
    protected $fillable = ['id','medicine_name','medicine_company','medicine_type','generic','medicine_code','medicine_rack','medicine_quantity','purchase_price','sell_price','medicine_effect','expire_date'];
}











