<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales_Master extends Model
{
    protected $table = 'sales_master';
    protected $fillable = ['sales_no','customer_id','discount','notes'];
}
