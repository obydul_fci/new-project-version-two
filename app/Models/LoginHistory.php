<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LoginHistory extends Model
{
    use SoftDeletes;
    protected $table = 'user_loghistory';
    protected $fillable = ['id','user_id','user_ip','browser_version','user_location','login_time','logout_time','status'];
    protected $dates = ['deleted_at'];
}
