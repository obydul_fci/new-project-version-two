<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales_Details extends Model
{
    protected $table = 'sales_details';
    protected $fillable = ['sales_id','sales_date','medicine_id','sales_quantity','sales_price','total_price','created_by','modified_by'];
}
