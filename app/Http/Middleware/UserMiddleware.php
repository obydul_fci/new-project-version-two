<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;
use Closure;
use Sentinel;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($user = Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='user')
        {
            // User is logged in and assigned to the `$user` variable.
            return $next($request);
        }
        else
        {
            // User is not logged in
            return redirect::to('user_login');
        }
    }
}
