<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MediaUpload;
use Session;
use File;
use DB;

class MediaUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $file = MediaUpload::latest()->paginate(10);
        return view('media_upload.index',compact('file'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $picture = '';
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $file->move('public/uploads/media', $filename);
                $picture = date('His').$filename;
                $data = MediaUpload::create(array(
                    'image' =>  $filename
                ));
            }
        }
        return redirect()->route('media_file.index')->with('success', 'Upload successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = DB::table('media_upload')->WHERE('id', $id)->first();
        $filepath = File::delete('public/uploads/media/' . $file->image);
        if ($filepath == true) {
            $data = DB::table('media_upload')->WHERE('id', $id)->delete();
            return redirect()->route('media_file.index')->with('success', 'File deleted successfully');
        }
    }
}
