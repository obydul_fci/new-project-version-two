<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\journal_master;

class JournalMaster extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $journals = journal_master::orderBy('id','DESC')->get();
        return view('account.journal_master',compact('journals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $journalMaster = journal_master::create(array(
            'journal_no' => $request->journal_no,
            'journal_date' => $request->journal_date,
            'journal_notes' => $request->journal_notes,
        ));
        return response()->json($journalMaster);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($journal_master_id)
    {
        $journalMaster = journal_master::find($journal_master_id);
        return response()->json($journalMaster);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $journal_master_id)
    {
        $journalMaster = journal_master::find($journal_master_id);
        $journalMaster->journal_no = $request->journal_no;
        $journalMaster->journal_date = $request->journal_date;
        $journalMaster->journal_notes = $request->journal_notes;
        $journalMaster->save();
        return response()->json($journalMaster);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($journal_master_id)
    {
        $journalMaster = journal_master::destroy($journal_master_id);
        return response()->json($journalMaster);
    }
}
