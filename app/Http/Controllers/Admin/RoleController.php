<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Role;
use App\Models\Permission;
use Sentinel;
use Session;
use DB;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $roles = Role::orderBy('id', 'DESC')->get();
                    return view('roles.index', compact('roles'));

                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    return view('roles.add');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {


                    $validator = Validator::make($request->all(), [
                        'slug' => 'required|unique:roles,slug|min:2|max:30',
                        'name' => 'required|unique:roles,name|min:2|max:30',
                        'permissions' => 'required'
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return redirect::to("roles/create")->withInput();
                    } else {
                        $role = new Role();
                        $permission = $request->get('permissions');
                        $permissionArr = explode(",", $permission);

                        $role->slug = $request->get('slug');
                        $role->name = $request->get('name');
                        $role->permissions = json_encode($permissionArr);
                        $role->save();
                        Session::flash('success', 'Successfully created Roles!');
                        return redirect::to('roles');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $role = Role::find($id);
                    return view('roles.edit', compact('role'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $roleId = Role::where('slug', $request->get('slug') || 'name', $request->get('name'))->first();
                    $validator = Validator::make($request->all(), [
                        'slug' => 'required|min:2|max:30|unique:roles,id,' . $id,
                        'name' => 'required|min:2|max:30',
                        'permissions' => 'required',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure Updated');
                        return Redirect::to('roles/' . $id . '/edit');
                    } else {
                        // store
                        $role = Role::find($id);
                        $permission = $request->get('permissions');
                        $permissionArr = explode(",", $permission);
                        $role->slug = $request->get('slug');
                        $role->name = $request->get('name');
                        $role->permissions = json_encode($permissionArr);
                        $role->save();

                        // redirect
                        Session::flash('success', 'Successfully updated role!');
                        return Redirect::to('roles');

                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = Role::where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the Roles!');
                    return redirect::to('roles');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }


}
