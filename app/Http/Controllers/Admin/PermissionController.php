<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Role;
use App\Models\Permission;
use Session;
use DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $permissions = Permission::orderBy('id', 'DESC')->get();
                    return view('permissions.index', compact('permissions'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    return view('permissions.add');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|unique:roles,name|min:2|max:30',
                        'display_name' => 'required|min:2|max:30',
                        'description' => 'required|min:2|max:30'
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return redirect::to("permissions/create")->withInput();
                    } else {
                        $permission = new Permission();
                        $permission->name = $request->get('name');
                        $permission->display_name = $request->get('display_name');
                        $permission->description = $request->get('description');
                        $permission->save();
                        Session::flash('success', 'Successfully created permission!');
                        return redirect::to('permissions');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $permission = Permission::find($id);
                    return view('permissions.edit', compact('permission'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|min:2|max:30',
                        'display_name' => 'required|min:2|max:30',
                        'description' => 'required|min:2|max:30'
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return Redirect::to('permissions/' . $id . '/edit');
                    } else {
                        $permission = Permission::find($id);
                        $permission->name = $request->get('name');
                        $permission->display_name = $request->get('display_name');
                        $permission->description = $request->get('description');
                        $permission->save();

                        // redirect
                        Session::flash('success', 'Successfully updated permission!');
                        return Redirect::to('permissions');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = Permission::where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the permission!');
                    return redirect::to('permissions');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }
}
