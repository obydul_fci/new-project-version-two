<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\coa_head;
use App\Models\User;
use DB;
use Input;
use Session;

class ChartAccountHead extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $res = DB::table('coa-head as t1')->join('coa-head AS t2', 't2.id', '=', 't1.parent')->select('t2.id','t2.acc_name','t2.parent')
//            ->where('t2.id','t1.parent')->get();
        $acc_head=coa_head::latest()->get();
        $acc_head_two = DB::table('coa-head')->where('parent',0)->get();
        $acc_head_three = DB::table('coa-head as t1')->join('coa-head AS t2', 't2.id', '=', 't1.parent')->select('t2.id','t2.acc_name','t2.parent')->get();
        return view('account.coa-head',compact('acc_head','acc_head_two','acc_head_three'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view_parent()
    {
        $acc_head = DB::table('coa-head')->get();
        $res=DB::table('coa-head as t1')->join('coa-head AS t2', 't2.id', '=', 't1.parent')->select('t2.id','t2.acc_name','t2.parent')->get();

        return view('account.coa-head',compact('acc_head','res'));
    }
//    public function just_test(){
//        $res = DB::table('coa-head as t1')->join('coa-head AS t2', 't2.id', '=', 't1.parent')->select('t2.id','t2.acc_name','t2.parent')->get();
//        dd($res);
//    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acc_head = coa_head::create(array(
            'acc_code' => $request->acc_code,
            'acc_name' => $request->acc_name,
            'acc_type' => $request->acc_type,
            'openning_balance' => $request->openning_balance,
            'parent' => $request->parent,
            'note' => $request->note,
            'status' => $request->status,
            'created_by' => $request->created_by,
            'modified_by' => $request->modified_by,
        ));
        return response()->json($acc_head);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($coa_id)
    {
        $acc_head =coa_head::find($coa_id);
        return response()->json($acc_head);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $coa_id)
    {
        $acc_head = coa_head::find($coa_id);
        $acc_head->acc_code = $request->acc_code;
        $acc_head->acc_name = $request->acc_name;
        $acc_head->acc_type = $request->acc_type;
        $acc_head->openning_balance = $request->openning_balance;
        $acc_head->parent = $request->parent;
        $acc_head->note = $request->note;
        $acc_head->status = $request->status;
        $acc_head->created_by = $request->created_by;
        $acc_head->modified_by = $request->modified_by;
        $acc_head->save();
        return response()->json($acc_head);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($coa_id)
    {
        $acc_head = coa_head::destroy($coa_id);
        return response()->json($acc_head);
    }
}
