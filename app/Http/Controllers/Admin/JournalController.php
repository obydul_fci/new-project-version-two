<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\journal_master;
use App\Models\journal_details;
use App\Models\coa_head;
use DB;
use Input;
use Session;
use redirect;
use Carbon\Carbon;
class JournalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acc_head=coa_head::latest()->get();
        $acc_head_two = DB::table('coa-head')->where('parent',0)->get();
        $acc_head_three = DB::table('coa-head as t1')->join('coa-head AS t2', 't2.id', '=', 't1.parent')->select('t2.id','t2.acc_name','t2.parent')->get();
        return view('account.journal',compact('acc_head','acc_head_two','acc_head_three'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $debit_total_amount=$request->total_debit_amounts;
        $credit_total_amount=$request->total_credit_amounts;
         if($credit_total_amount!=$debit_total_amount){
             echo "not equal";
             return redirect()->back();
         }else{
        $request->session()->flash('status', 'Task was successful!');
        $j_no = $request->get('journal_no');
        $journal_date = $request->get('date');
        $notes = $request->get('notes');
        $user=$request->get('user_type');
        $status = (isset($_POST['onoffswitch'])) ? 1 : 0;
        $current_time = Carbon::now()->toDateTimeString();
        DB::table('journal-master')->insert(
            [
                'journal_no' => $j_no,
                'journal_date' => $journal_date,
                'notes' => $notes,
                'status'=> $status,
                'created_by' =>$user,
                'modified_by'=>$user,
                'created_at'=>$current_time,
                'updated_at'=>$current_time,
            ]
        );
        $purchaseId = DB::getPdo()->lastInsertId();
        $rows = $request->get('rows');
        $roww = $request->get('roww');
        $data_to_insert = array();
        $data_to_arr=array();
        foreach ($rows as  $row){
            $data_to_insert[] = [
                'journal_id' => $purchaseId,
                'coa_id' => $row['debit_acc_name'],
                'debit' => $row['debit_amount_name'],
                'created_by' =>$user,
                'modified_by'=>$user,
                'created_at'=>$current_time,
                'updated_at'=>$current_time,
            ];
        }
        foreach ($roww as  $rowrrr){
            $data_to_arr[] = [
                'journal_id'=> $purchaseId,
                'coa_id'=>$rowrrr['credit_acc_name'],
                'credit' => $rowrrr['credit_amount_name'],
                'created_by' =>$user,
                'modified_by'=>$user,
                'created_at'=>$current_time,
                'updated_at'=>$current_time,
            ];
        }
        DB::table('journal-details')->insert($data_to_insert);
        DB::table('journal-details')->insert($data_to_arr);
        return redirect()->back();
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
