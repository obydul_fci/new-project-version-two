<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MedicineModel;
use App\Models\Generics;
use Session;
use DB;


class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $medicine = MedicineModel::latest()->get();
          $generics=DB::table('generics')->get();
          return view('medicine.index',compact('medicine','generics'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $medicine = MedicineModel::create($request->input());
        $medicine = MedicineModel::create(array(
            'medicine_name' => $request->medicine_name,
            'medicine_company' => $request->medicine_company,
            'medicine_type' => $request->medicine_type,
            'generic' => $request->generic,
            'medicine_code' => $request->medicine_code,
            'medicine_rack' => $request->medicine_rack,
            'medicine_quantity' => $request->medicine_quantity,
            'purchase_price' => $request->purchase_price,
            'sell_price' => $request->sell_price,
            'medicine_effect' => $request->medicine_effect,
            'expire_date' => $request->expire_date,
        ));
        return response()->json($medicine);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {
        $medicine =MedicineModel::find($product_id);
        return response()->json($medicine);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        $medicine = MedicineModel::find($product_id);
        $medicine->medicine_name = $request->medicine_name;
        $medicine->medicine_company = $request->medicine_company;
        $medicine->medicine_type = $request->medicine_type;
        $medicine->generic = $request->generic;
        $medicine->medicine_code = $request->medicine_code;
        $medicine->medicine_rack = $request->medicine_rack;
        $medicine->medicine_quantity = $request->medicine_quantity;
        $medicine->purchase_price = $request->purchase_price;
        $medicine->sell_price = $request->sell_price;
        $medicine->medicine_effect = $request->medicine_effect;
        $medicine->expire_date = $request->expire_date;
        $medicine->save();
        return response()->json($medicine);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product_id)
    {
        $medicine = MedicineModel::destroy($product_id);
        return response()->json($medicine);
    }
}
