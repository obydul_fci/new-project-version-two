<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Role;
use App\Models\RoleAssign;
use App\User;
use Session;
use DB;

class RoleAssiginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $roleAssigns = DB::table('users')
                        ->leftJoin('role_users', 'users.id', '=', 'role_users.user_id')
                        ->join('roles', 'role_users.role_id', '=', 'roles.id')
                        ->select('role_users.id', 'users.full_name AS user_name', 'roles.name AS role_name')->orderBy('role_users.id','DESC')->get();
                    return view('role_assigns.index', compact('roleAssigns'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $user = User::where('status',1)->get();
                    $roles = Role::get();
                    return view('role_assigns.add', compact('user', 'roles'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'user_id' => 'required',
                        'role_id' => 'required',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return redirect::to("roleAssigns/create")->withInput();
                    } else {
                        $cheackValu = $request->get('role_id');
                        $cheack = DB::table('roles')->where('id', $cheackValu)->first();

                        if ($cheack->slug == 'supper_admin') {
                            Session::flash('error', 'Supper Admin only one..already exist');
                            return redirect::to("roleAssigns/create")->withInput();
                        } else {
                            $roleAssign = new RoleAssign();
                            $roleAssign->user_id = $request->get('user_id');
                            $roleAssign->role_id = $request->get('role_id');
                            $roleAssign->save();
                            Session::flash('success', 'Successfully created Roles!');
                            return redirect::to('roleAssigns');
                        }

                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $user = User::get();
                    $roles = Role::get();
                    $roleUser = RoleAssign::find($id);
                    return view('role_assigns.edit', compact('user', 'roles', 'roleUser'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'user_id' => 'required|unique:role_users,id,' . $id,
                        'role_id' => 'required',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure Updated');
                        return Redirect::to('roleAssigns/' . $id . '/edit');
                    } else {
                        $cheackValu = $request->get('role_id');
                        $cheack = DB::table('roles')->where('id', $cheackValu)->first();

                        if ($cheack->slug == 'supper_admin') {
                            Session::flash('error', 'Supper Admin only one..already exist');
                            return Redirect::to('roleAssigns/' . $id . '/edit');
                        } else {
                            $roleAssign = RoleAssign::find($id);
                            $roleAssign->user_id = $request->get('user_id');
                            $roleAssign->role_id = $request->get('role_id');
                            $roleAssign->save();
                            Session::flash('success', 'Successfully updated role!');
                            return Redirect::to('roleAssigns');
                        }


                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = RoleAssign::where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the Roles!');
                    return redirect::to('roleAssigns');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }
}
