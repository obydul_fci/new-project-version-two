<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Role;
use App\Models\PermissionAssign;
use App\Models\Permission;
use App\User;
use Session;
use DB;

class PermissionAssiginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $permissionAssigns = DB::table('users')
                        ->join('permission_role', 'users.id', '=', 'permission_role.user_id')
                        ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                        ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                        ->select('permission_role.*', 'roles.name AS role_name', 'permissions.name AS permission_name', 'users.full_name')->orderBy('permission_role.id', 'DESC')->get();
                    return view('permission_assigns.index', compact('permissionAssigns'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $users = DB::table('users')->join('activations', 'users.id', '=', 'activations.user_id')->where('activations.completed', 1)->select('users.*')->get();
                    $roles = Role::get();
                    $permissions = Permission::get();
                    return view('permission_assigns.add', compact('users', 'permissions', 'roles'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'user_id' => 'required',
                        'permission_id' => 'required|unique:permission_role,permission_id',
                        'role_id' => 'required',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return redirect::to("permissionAssigns/create")->withInput();
                    } else {
                        $permissionAssign = new PermissionAssign();
                        $permissionAssign->user_id = $request->get('user_id');
                        $permissionAssign->permission_id = $request->get('permission_id');
                        $permissionAssign->role_id = $request->get('role_id');
                        $permissionAssign->save();
                        Session::flash('success', 'Successfully created Roles!');
                        return redirect::to('permissionAssigns');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $users = DB::table('users')->join('activations', 'users.id', '=', 'activations.user_id')->where('activations.completed', 1)->select('users.*')->get();
                    $permissions = Permission::get();
                    $roles = Role::get();
                    $permissionAssign = PermissionAssign::find($id);
                    return view('permission_assigns.edit', compact('users', 'permissions', 'roles', 'permissionAssign'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'permission_id' => 'required',
                        'role_id' => 'required',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Request Failure');
                        return Redirect::to('permissionAssigns/' . $id . '/edit');
                    } else {
                        $permissionAssign = PermissionAssign::find($id);
                        $permissionAssign->permission_id = $request->get('permission_id');
                        $permissionAssign->role_id = $request->get('role_id');
                        $permissionAssign->save();

                        // redirect
                        Session::flash('success', 'Successfully updated Permission Role!');
                        return Redirect::to('permissionAssigns');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = PermissionAssign::where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the Permission Role !');
                    return redirect::to('permissionAssigns');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }
}
