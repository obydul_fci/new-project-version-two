<?php

namespace App\Http\Controllers\Admin;
//namespace Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\LoginHistory;
use Carbon\Carbon;
use App\User;
use Socialite;
use Sentinel;
use Activation;
use Session;
use Auth;
use DB;
date_default_timezone_set("Asia/Dhaka");
class UserLoginController extends Controller
{
    public function create(){
        return view ('auth.login');
    }
    public function userLogin(Request $request){

        $user = Sentinel::findById(1);

//        if ($activation = Activation::completed($user))
        if($user = Sentinel::authenticate($request->only('email', 'password'), $request->has('remember')))
        {
            if (Sentinel::getUser()->status==1){
            $userLogin = new LoginHistory();
            $userLogin->user_id = $user->id;
            $userLogin->user_ip = $request->ip();
            $userLogin->browser_version =  $request->server('HTTP_USER_AGENT');
            $userLogin->login_time = date("h:i:s A");
            $userLogin->status = 'Login...';
            $userLogin->save();

            return redirect::to('dashboard');
            }else{
                Session::flash('error', 'you are not active.please contact administrator');
                return redirect::to('user_login');
            }
        }
        else
        {
            Session::flash('error', 'Email or password you entered is incorrect');
            return redirect::to('user_login');
        }

    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();

            $check = User::where('email',$user->email)->first();
            if($check){
                Auth::login($check);
                return redirect::to('/');

            }else{
                $userModel = new User;
                $userModel->name = $user->name;
                $userModel->email = $user->email;
                $userModel->save();

                Auth::login($userModel);
                return redirect::to('/');
            }

        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
    }

    public function redirectToLinkedin()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function handleLinkedinCallback()
    {
        try {
            $user = Socialite::driver('linkedin')->user();

            $check = User::where('email',$user->email)->first();
            if($check){
                Auth::login($check);
                return redirect::to('/');

            }else{
                $userModel = new User;
                $userModel->name = $user->name;
                $userModel->email = $user->email;
                $userModel->save();

                Auth::login($userModel);
                return redirect::to('/');
            }
        } catch (Exception $e) {
            return redirect('auth/linkedin');
        }
    }
    public function userLogOut(){

        $userLogout = $user = Sentinel::getUser()->id;
        $userInfo = DB::table('user_loghistory')->where('user_id',$userLogout && 'logout_time','NULL')
            ->orderBy('id','DESC')->update(['logout_time'=>date("h:i:s A"),'status'=>'Logout']);
        Sentinel::logout();
        return redirect('user_login');
    }

}
