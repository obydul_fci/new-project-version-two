<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Session;
use redirect;
use Carbon\Carbon;

class sales extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customer = DB::table('permission_role')
            ->join('users', 'users.id', '=', 'permission_role.user_id')
            ->join('roles', 'roles.id', '=', 'permission_role.role_id')
            ->select('users.*', 'roles.name AS Role_name','users.full_name AS Customer_name','users.id AS Customer_id')
            ->Where('roles.name','=','Customer')
            ->get();
       $medicine=DB::table('medicine')->get();
       return view('account.sales',compact('medicine','customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request->session()->flash('success', 'Sales was successful!');
            $s_no = $request->get('sales_no');
            $customer_id = $request->get('customer_id');
            $discount = $request->get('discount');
            $sales_date = $request->get('date');
            $notes = $request->get('sale_notes');
            $user=$request->get('user_type');
            $sale_sum_amount=$request->get('sale_sum_amount_price');
            $current_time = Carbon::now()->toDateTimeString();
            DB::table('sales_master')->insert(
                [
                    'sales_no' => $s_no,
                    'customer_id' => $customer_id,
                    'discount' => $discount,
                    'total_sum' => $sale_sum_amount,
                    'sales_date' => $sales_date,
                    'notes' => $notes,
                    'created_by' =>$user,
                    'modified_by'=>$user,
                    'created_at'=>$current_time,
                    'updated_at'=>$current_time,
                ]
            );
            $purchaseId = DB::getPdo()->lastInsertId();
            $rows = $request->get('rows');
            $data_to_insert = array();
            foreach ($rows as  $row){
                $data_to_insert[] = [
                    'sales_id' => $purchaseId,
                    'sales_date' =>$sales_date,
                    'medicine_id' => $row['medicine_name'],
                    'sales_price' => $row['sales_price_id'],
                    'sales_quantity' => $row['sales_quantity'],
                    'total_price' => $row['sales_total_amount'],
                    'created_by' =>$user,
                    'modified_by'=>$user,
                    'created_at'=>$current_time,
                    'updated_at'=>$current_time,
                ];
            }
            DB::table('sales_details')->insert($data_to_insert);
            return redirect()->back();
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = DB::table('medicine')->where('id',$id)->get();
       return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
