<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Models\Role;
use Session;
Use DB;
use App\Models\User;

class UserActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $users = DB::table('users')
                        ->join('role_users', 'users.id', '=', 'role_users.user_id')
                        ->join('roles', 'role_users.role_id', '=', 'roles.id')->
                        select('users.*', 'roles.name AS role_name')->orderBy('users.id', 'DESC')->get();
                    return view('user_activation.index', compact('users'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $user_activation = DB::table('users')->where('users.id', $id)
                        ->join('role_users', 'users.id', '=', 'role_users.user_id')
                        ->join('roles', 'role_users.role_id', '=', 'roles.id')->
                        select('users.*', 'roles.name AS role_name')->orderBy('users.id', 'DESC')->first();
                    return view('user_activation.show', compact('user_activation'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_activations = DB::table('users')->where('users.id', $id)
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')->
            select('users.*', 'roles.name AS role_name')->orderBy('users.id', 'DESC')->first();
           return view('user_activation.active', compact('user_activations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $activation = DB::table('users')->where('id', $id)->update(['status' => $request->get('status')]);
                    Session::flash('success', 'Successfully updated role!');
                    return Redirect::to('user_activation');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
