<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Models\Role;
use App\Models\LoginHistory;
use Session;
use DB;

class UserLogHistoryController extends Controller
{
    //user login history show (group by)
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $userHistorys = DB::table('user_loghistory')->join('users', 'user_loghistory.user_id', '=', 'users.id')
                        ->select('users.full_name', 'user_loghistory.*')->orderBy('user_loghistory.id', 'DESC')->groupBy('user_loghistory.user_id')->get();
                    return view('user_history.index', compact('userHistorys'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    // user login history show single user
    public function singleUserHistory($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $singleUserHistory = DB::table('user_loghistory')->where('user_loghistory.user_id', $id)
                        ->join('users', 'user_loghistory.user_id', '=', 'users.id')
                        ->select('users.full_name', 'user_loghistory.*')->orderBy('user_loghistory.id', 'DESC')->get();
                    return view('user_history.user_history_details', compact('singleUserHistory'));
                }
            }
        } else {
            return view('error_page.error_404');
        }

    }

    // permanently delete
    public function permanentlyDelete($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = DB::table('user_loghistory')->where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the Roles!');
                    return redirect()->back();
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }
}
