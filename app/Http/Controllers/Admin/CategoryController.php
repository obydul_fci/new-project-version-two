<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Role;
use App\Models\Main_Category;
use Session;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // min category show
    public function index()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $mainCategorys = DB::table('main_category')->where('deleted_at', NULL)->orderBy('id', 'DESC')->get();
                    $parentCategorys = DB::table('main_category')->join('category_textsonomy','main_category.id','=','category_textsonomy.parent')
                        ->where('main_category.deleted_at', NULL)->orderBy('main_category.id', 'DESC')
                        ->select('main_category.slug','category_textsonomy.texsonomy')->get();
                    return view('categorys.main_categorys', compact('mainCategorys'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function main_category_create()
//    {

//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //main category store
    public function store(Request $request)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('create' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'category_name' => 'required|min:2|max:30|unique:main_category',
                        'slug' => 'required|min:2|max:30|unique:main_category',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Category and slug name should be unique...');
                        return redirect::back()->withInput();
                    } else {
                        $main_category = new Main_Category();
                        $main_category->category_name = $request->get('category_name');
                        $main_category->slug = $request->get('slug');
                        $main_category->save();
                        if ($main_category) {
                            $texsonomoy = DB::table('category_textsonomy')->insert(['category_id' => $main_category->id,
                                'texsonomy' => $request->get('slug'),
                                'parent' => $request->get('parent_id'),
                                'count' => '0']);
                        }
                        Session::flash('success', 'Successfully created Category!');
                        return redirect::to('categorys');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    // mian category updated
    public function update(Request $request, $id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('update' == $cleanStr) {
                    $validator = Validator::make($request->all(), [
                        'category_name' => 'required|min:2|max:30',
                        'slug' => 'required|min:2|max:30',
                    ]);
                    if ($validator->fails()) {
                        Session::flash('error', 'Category and slug name should be unique...');
                        return Redirect::back();
                    } else {
                        // store
                        $categorys = Main_Category::find($id);
                        $categorys->category_name = $request->get('category_name');
                        $categorys->slug = $request->get('slug');
                        $categorys->save();
                        if ($categorys) {
                            $texsonomoy = DB::table('category_textsonomy')->where('category_id', $id)->update(['parent' => $request->get('parent_id')]);
                        }

                        // redirect
                        Session::flash('success', 'Successfully updated Category!');
                        return Redirect::to('categorys');
                    }
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    // main category active inactive settings
    public function main_category_activation(Request $request, $id)
    {
        $activation = DB::table('main_category')->where('id', $id)->update(['status' => $request->get('status')]);
        Session::flash('success', 'Successfully updated Status!');
        return Redirect::to('categorys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    // min category delete
    public function destroy($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = Main_Category::where('id', $id)->delete();
                    $texsonomeyDelete = DB::table('category_textsonomy')->where('category_id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the Category!');
                    return redirect::to('categorys');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    // sub category show
    public function subcategory()
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('view' == $cleanStr) {
                    $subCategorys = DB::table('category_textsonomy')
                        ->join('main_category', 'category_textsonomy.category_id', '=', 'main_category.id')
                        ->select('category_textsonomy.*', 'main_category.category_name', 'main_category.slug')->orderBy('category_textsonomy.id', 'DESC')->get();
                    $categorys = DB::table('main_category')->get();
                    return view('categorys.sub_categorys', compact('subCategorys', 'categorys'));
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }

    // sub category deleted
    public function sub_category_delete($id)
    {
        $userPermission = new Role();
        $userPermissionAccess = $userPermission->userAccessShare();
        $array = json_decode($userPermissionAccess->permissions);
        if (is_array($array) || is_object($array)) {
            foreach ($array as $value) {
                $cleanStr = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $value)));
                if ('delete' == $cleanStr) {
                    $delete = DB::table('category_textsonomy')->where('id', $id)->delete();
                    Session::flash('success', 'Successfully deleted the sub Category!');
                    return redirect::to('subcategorys');
                }
            }
        } else {
            return view('error_page.error_404');
        }
    }


}
