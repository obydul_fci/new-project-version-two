<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
class SalesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('account.sales_returns');
    }

    public function invoiceWiseMedicine(Request $request)
    {
        $search = $request->id;
        $medicines  = DB::table('sales_details')->where('sales_id',$search)
            ->join('medicine','sales_details.medicine_id','=','medicine.id')
            ->select('medicine.id','medicine.medicine_name')->groupBy('medicine.medicine_name')->get();
        return response()->json($medicines);

    }

    public function invoiceWiseMedicineDetails(Request $request)
    {
        $salesReturnNo = $request->id;
        $medicines = $request->id2;
        $medicines  = DB::table('sales_details')->where('sales_id',$salesReturnNo)->where('medicine_id',$medicines)
            ->join('sales_master','sales_details.sales_id','=','sales_master.sales_no')
            ->join('medicine','sales_details.medicine_id','=','medicine.id')
            ->select('medicine.id','medicine.medicine_name','sales_master.customer_id','sales_details.sales_id','sales_details.medicine_id','sales_details.sales_quantity','sales_details.sales_price','sales_details.total_price')->get();
        return response()->json($medicines);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $selseReturnNo = $request->get('selseReturnNo');
        $salesReturnDate = $request->get('salesReturnDate');
        $notes = $request->get('notes');
        $customerId = $request->get('customerId');
        DB::table('sales_return_masters')->insert(
            [
                'sales_return_no' => $selseReturnNo,
                'sales_return_date' => $salesReturnDate,
                'customer_id' => $customerId,
                'notes' => $notes
            ]
        );

        $rows = $request->get('rows');
        $data_to_insert = array();
        foreach ($rows as  $row){
            $data_to_insert[] = [
                'sales_return_no' =>$selseReturnNo,
                'sales_return_date' => $salesReturnDate,
                'medicine_id' => $row['salesMedicineId'],
                'quantity' => $row['salesQuantity'],
                'sales_price' => $row['sales_price'],
                'total_price' => $row['salesTotalPrice']
            ];
        }
        DB::table('sales_return_details')->insert($data_to_insert);

        Session::flash('success', 'Successfully updated role!');
        return Redirect::to('salesReturn');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
