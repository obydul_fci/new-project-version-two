<?php

namespace App\Http\Controllers\Admin;

use App\Models\MediaUpload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use DB;
use Redirect;
use Session;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile =DB::table('users')->where('id',Sentinel::getUser()->id)->first();
        return view('profile.index',compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $media = MediaUpload::all();
        $profile =DB::table('users')->where('id',Sentinel::getUser()->id)->first();
        return view('profile.edit',compact('profile','media'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //if has user custom image file then update user profile
        if ($request->hasFile('user_profile_image')) {
            $id=Sentinel::getUser()->id;
            $files = $request->file('user_profile_image');
                $filename = $files->getClientOriginalName();
                $files->move('public/uploads/media', $filename);
                $update = DB::table('users')->where('id', $id)->update([
                  'picture' =>$filename,
                 ]);
                if($update){
                    $insert =  DB::table('media_upload')->insert(
                        array(
                            'image' => $filename,
                            'created_at' => \Carbon\Carbon::now(),
                            'updated_at' => \Carbon\Carbon::now()
                        )
                    );
                }
                 return Redirect::back()->with('success','profile image update successfully');
            }
           //if checkbox is selected then update user profile image from media upload
            if (isset($_POST['check_list'])) {
                $name=$_POST['check_list'];
                $id=Sentinel::getUser()->id;
                $update = DB::table('users')->where('id', $id)->update([
                    'picture' =>$name,
                ]);
                return Redirect::back()->with('success','profile image update successfully');
            }
            //user profile update
            if(isset($_POST['profile-info-update'])){
                $id=Sentinel::getUser()->id;
                $update = DB::table('users')->where('id', $id)->update([
                    'full_name'   => $request->input('name'),
                    'gender'      => $request->input('g'),
                    'profession'  => $request->input('profession'),
                    'division'    => $request->input('division'),
                    'district'    => $request->input('district'),
                    'upzila'      => $request->input('upazila'),
                    'mobile'      => $request->input('mobile'),
                    'address'     => $request->input('address'),
                    'birth_date'  => $request->input('date'),
                ]);
                return redirect()->action('Admin\UserProfileController@index')->with('success','Update successfully');
            }
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
