<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\User;
use Socialite;
use Sentinel;
use Activation;
use Session;
use Auth;
class UserRegistrationController extends Controller
{
    public function userRegForm(){
    	return view('auth.register');
    }

    protected function userRegStore(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('error', 'Request Failure');
            return redirect::to("user_register")->withInput();
        } else {
            $pass = $request->get('password');
            $conPass = $request->get('password_confirmation');
            if($pass==$conPass){

                $user = Sentinel::registerAndActivate(array(
                    'full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'password' => $request['password'],
                    'gender' => $request['gender'],
                ));
                $role = Sentinel::findRoleBySlug('user');
                if (isset($role)) {
                    $role->users()->attach($user);
                }
                Session::flash('success', 'Successfully Registation!');
                return redirect::to('user_login');

            }else{
                Session::flash('success', 'Password not meatch');
                return redirect::to('user_reg');
            }
        }

    }

    protected function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('error', 'Request Failure');
            return redirect::to("user_register")->withInput();
        } else {
            $pass = $request->get('password');
            $conPass = $request->get('password_confirmation');
            if($pass==$conPass){

                $user = Sentinel::registerAndActivate(array(
                    'full_name' => $request['full_name'],
                    'email' => $request['email'],
                    'password' => $request['password'],
                    'gender' => $request['gender'],
                ));
                $role = Sentinel::findRoleBySlug('user');
                if (isset($role)) {
                    $role->users()->attach($user);
                }
                Session::flash('success', 'Successfully Registation!');
                return redirect::to('user_activation');

            }else{
                Session::flash('success', 'Password not meatch');
                return redirect::to('user_activation');
            }
        }

    }
}
