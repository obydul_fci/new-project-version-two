<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Generics;
use Sentinel;
use Session;
use DB;
class GenericsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genericsList = DB::table('generics')->orderBy('id','DESC')->get();
        return view('generics.index',compact('genericsList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'generic_name' => 'required|unique:generics,generic_name|min:2|max:30',
        ]);
        if ($validator->fails()) {
            Session::flash('error', 'Request Failure');
            return redirect::to("generics")->withInput();
        } else {
            $generics = new Generics();
            $generics->generic_name = $request->get('generic_name');
            $generics->save();
            Session::flash('success', 'Successfully created Generic!');
            return redirect::to('generics');
        }
    }
    public function fileStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'generic_name' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('error', 'Request Failure');
            return redirect::to("generics")->withInput();
        } else {
            $upload = $request->file('generic_name');
            $csvFile = file_get_contents($upload);
            $row = array_map('str_getcsv',explode("\n",$csvFile));
            $header = array_shift($row);
            foreach ($row as $key => $value){
                $data = array_combine($header,$value);
                Generics::create([
                    'generic_name'=>$data['generic_name'],
                ]);
            }
            Session::flash('success', 'Successfully created Generic!');
            return redirect::to('generics');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'generic_name' => 'required|min:2|max:30|unique:generics,id,' . $id,
        ]);
        if ($validator->fails()) {
            Session::flash('error', 'Request Failure Updated');
            return back();
        } else {
            // store
            $generics = Generics::find($id);
            $generics->generic_name = $request->get('generic_name');
            $generics->save();
            // redirect
            Session::flash('success', 'Successfully updated Generic!');
            return Redirect::to('generics');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Generics::where('id', $id)->delete();
        Session::flash('success', 'Successfully deleted the Generic!');
        return redirect::to('generics');
    }
}
