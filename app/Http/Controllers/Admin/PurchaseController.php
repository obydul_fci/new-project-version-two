<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = DB::table('roles')->where('slug','supplier')
            ->join('role_users','roles.id','=','role_users.role_id')
            ->join('users','role_users.user_id','=','users.id')->select('users.id','users.full_name')->orderBy('users.id','DESC')
            ->get();
        $medicines = DB::table('medicine')->get();
        return view('account.purchase',compact('suppliers','medicines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $invoice_no = $request->get('invoice_no');
        $supplier_name = $request->get('supplier_name');
        $notes = $request->get('notes');
        $purchase_date = $request->get('purchase_date');


        $modal_medicine_name = $request->get('modal_medicine_name');
        $modal_medicine_quantity = $request->get('modal_medicine_quantity');
        $modal_discount_price = $request->get('modal_discount_price');


         DB::table('purchase_masters')->insert(
            [
                'supplier_id' => $supplier_name,
                'invoice_no' => $invoice_no,
                'purchase_date' => $purchase_date,
                'notes' => $notes
            ]
        );
        $purchaseId = DB::getPdo()->lastInsertId();
        $rows = $request->get('rows');
        $data_to_insert = array();
        foreach ($rows as  $row){
            $data_to_insert[] = [
                'medicine_id' =>$row['modal_medicine_name'],
                'purchase_id' => $purchaseId,
                'invoice_no' => $invoice_no,
                'quantity' => $row['modal_medicine_quantity'],
                'purchase_price' => $row['modal_discount_price']
            ];
        }
        DB::table('purchase_details')->insert($data_to_insert);
        // insert query
        DB::table('journal-master')->insert(
            [
                'journal_no' => $invoice_no,
                'journal_date' => $purchase_date,
                'journal_notes' => $notes
            ]
        );
        if (isset($_POST['remember_me_1']) && isset($_POST['remember_me_2']) && isset($_POST['remember_me_3'])){
            $cashPayment = $_POST['cash_payment'];
            $bankPayment = $_POST['bank_payment'];
            $buyRemaining = $_POST['buy_remaining'];

            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1002,
                    'credit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1003,
                    'credit' => $bankPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1004,
                    'credit' => $buyRemaining,
                ]
            );
        } elseif (isset($_POST['remember_me_2']) && isset($_POST['remember_me_3'])){
            $bankPayment = $_POST['bank_payment'];
            $buyRemaining = $_POST['buy_remaining'];
            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $bankPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1003,
                    'credit' => $bankPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1004,
                    'credit' => $buyRemaining,
                ]
            );
        } elseif (isset($_POST['remember_me_1']) && isset($_POST['remember_me_3'])){

            $cashPayment = $_POST['cash_payment'];
            $buyRemaining = $_POST['buy_remaining'];
            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1002,
                    'credit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1004,
                    'credit' => $buyRemaining,
                ]
            );
        }elseif (isset($_POST['remember_me_1']) && isset($_POST['remember_me_2'])){
            $cashPayment = $_POST['cash_payment'];
            $bankPayment = $_POST['bank_payment'];
            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1002,
                    'credit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1003,
                    'credit' => $bankPayment,
                ]
            );

        }elseif (isset($_POST['remember_me_1'])){
            //journal details table data store
            $cashPayment = $_POST['cash_payment'];

            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $cashPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1002,
                    'credit' => $cashPayment,
                ]
            );
        }elseif (isset($_POST['remember_me_2']) ){
            //journal details table data store
            $bankPayment = $_POST['bank_payment'];

            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $bankPayment,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1003,
                    'credit' => $bankPayment,
                ]
            );
        }elseif (isset($_POST['remember_me_3'])){
            //journal details table data store
            $buyRemaining = $_POST['buy_remaining'];
            $purchaseId = DB::getPdo()->lastInsertId();
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1001,
                    'debit' => $buyRemaining,
                ]
            );
            DB::table('journal-details')->insert(
                [
                    'journal_id' => $purchaseId,
                    'coa_id' => 1004,
                    'credit' => $buyRemaining,
                ]
            );
        }
        Session::flash('success', 'Successfully updated role!');
        return Redirect::to('purchase');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
